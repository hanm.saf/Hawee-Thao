<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'develop_wpbase');

/** MySQL database username */
define('DB_USER', 'develop_wpbase');

/** MySQL database password */
define('DB_PASSWORD', 'base!@#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Disallow admin to edit plugins, theme */
define('DISALLOW_FILE_EDIT', true);
define('DISALLOW_FILE_MODS', true);

/** Disable auto update core */
define('WP_AUTO_UPDATE_CORE', false);
define('WP_POST_REVISIONS', false);
/* * #@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Zf2WBnzdYER,-XJ1?lKe-HjdnAjUE;!3fk;Z+oV(Y6KnIktqu(`K#<L-Y.R1 q:q');
define('SECURE_AUTH_KEY', '!RG|!RHW32EpFsyeW|z-{X4>K?USt^MKgBXis+?@,-^jiW[.Jfba]ttt7Yxe,E&e');
define('LOGGED_IN_KEY', 'u@nj!{$JJ!:~|s~KQ.Lz)+D@-o-7YL-1$4+oZ}+6QhD-J$tG)*;}Bly+58>:U*.p');
define('NONCE_KEY', 'Z{8#Ax*KPk)Ggs4,(IN3 mQjKp6/*%:,X/^z3Rv/|(vS.EKA5y`f0J>dl]Ad-!uD');
define('AUTH_SALT', 'ua@pIK0B}N8t[,&)S95~ r9Ej1GA3]&Dae!MZ@hX>9.U*2G0ox[ts~qu((iD}Y~q');
define('SECURE_AUTH_SALT', 'z6rM}9XF&k)jR|w-q>pmli>uNWA6cX.ja!E|_4=R#Tk $L}-]#sG-% <D<i$~zkn');
define('LOGGED_IN_SALT', '81cefYe}F/pt(kxhis]mF$b%Wbo[.,+P^Tw3<](-}u[D: X!7#B!:0X4_{X|RQS1');
define('NONCE_SALT', 'fAfm^cML$|wpm:T2E$){c3hDM*eWP<]+d9K9bR+ElH3}t~|I-p6ZN 0f:RMU[5--');

/* * #@- */

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tu_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
