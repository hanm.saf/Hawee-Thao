<?php
//  Template Name: Contact Page
get_header();
?>

<?php
/* Current Page Info */
$post_object = $wp_query->get_queried_object();
$post_id = $post_object->ID;
$post_permalink = get_permalink($post_id);
$post_title = $post_object->post_title;
$post_excerpt = $post_object->post_excerpt;
$post_content = $post_object->post_content;
$post_thumbnail_url = get_the_post_thumbnail_url($post_id);

/**
 * Get theme options
 * Date: 08/02/2017
 */
$theme_hotline = get_option('theme_hotline');
$theme_phone = get_option('theme_phone');
$theme_fax = get_option('theme_fax');
$theme_email = get_option('theme_email');
$theme_address = get_option('theme_address');
?>

<div class="container">
    <div class="row">
        <h2 class="text-center"><?php echo $post_title; ?></h2>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <!--Theme options address-->
            <?php if ($theme_address != "") { ?>
                <h3><strong><?php _e('Địa chỉ', TEXT_DOMAIN); ?></strong>: <?php echo $theme_address; ?></h3>
            <?php } ?>
            <!--Theme options phone-->
            <?php if ($theme_phone != "") { ?>
                <p><strong><?php _e('Điện thoại', TEXT_DOMAIN); ?></strong>: <a href="tel:<?php echo $theme_phone; ?>"><?php echo $theme_phone; ?></a></p>
            <?php } ?>
            <!--Theme options email-->
            <?php if ($theme_email != "") { ?>
                <p><strong>Email</strong>: <a href="mailto:<?php echo $theme_email; ?>"><?php echo $theme_email; ?></a></p>
            <?php } ?>
            <!--Theme options holine-->
            <?php if ($theme_hotline != "") { ?>
                <p><strong>Holine</strong>: <a href="tel:<?php echo $theme_hotline; ?>"><?php echo $theme_hotline; ?></a></p>
            <?php } ?>
            <!--Theme options fax-->
            <?php if ($theme_fax != "") { ?>
                <p><strong>Fax</strong>: <a href="tel:<?php echo $theme_fax; ?>"><?php echo $theme_fax; ?></a></p>
            <?php } ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?php get_template_part('partials/block', 'form-contact-submit'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>