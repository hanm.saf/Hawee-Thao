<?php
if (!session_id()) {
    session_start();
}
//  Template Name: Facebook Callback Page 
/** Current Page Info */
$current_page_object = $wp_query->get_queried_object();
$current_page_url = $current_page_object->guid;
$current_page_id = $current_page_object->ID;
$current_page_title = $current_page_object->post_title;
$current_page_excerpt = $current_page_object->post_excerpt;
$current_page_content = $current_page_object->post_content;
$current_page_thumbnail_url = get_the_post_thumbnail_url($current_page_id);

/** Facebook Initialize */
include_once(TEMPLATE_PATH . '/includes/vendor/facebook/graph-sdk/src/Facebook/autoload.php');

$fb = new Facebook\Facebook([
    'app_id' => FACEBOOK_APP_ID,
    'app_secret' => FACEBOOK_APP_SECRET,
    'default_graph_version' => 'v2.5',
        ]);


$fb_login_helper = $fb->getRedirectLoginHelper();
//$fb_login_permissions = ['email', 'user_likes'];
//$fb_login_callback_url = add_query_arg(array('action' => 'callback'), $current_page_url);
//$fb_login_url = $fb_login_helper->getLoginUrl($fb_login_callback_url, $fb_login_permissions);

try {
    $accessToken = $fb_login_helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

try {
    $response = $fb->get('/me?fields=id,name,email,gender,picture', $accessToken->getValue());
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    echo 'ERROR: Graph ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo 'ERROR: validation fails ' . $e->getMessage();
    exit;
}
$user = $response->getGraphUser();
$user_id = $user->getProperty('id');
$user_name = $user->getProperty('name');
$user_email = $user->getProperty('email') ? $user->getProperty('email') : $user_id . "@facebook.com";
$user_gender = ($user->getProperty('gender') == "male") ? "Nam" : "Nữ";
$user_avatar = $user->getProperty('picture');

echo "ID: " . $user_id . "<br>";
echo "Full Name: " . $user_name . "<br>";
echo "Email: " . $user_email . "<br>";
echo "Giới tính: " . $user_gender . "<br>";
echo "Ảnh đại diện" . "<br />";
?>

<img src="<?php echo $user_avatar['url']; ?>" />


