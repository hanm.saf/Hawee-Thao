<?php
//  Template Name: Article Page
get_header();
?>
<?php//echo tu_get_css_page_template("article-page", TEMPLATE_URL . "/assets/stylesheets/sass/pages/article.css");  ?>
<?php
/* Current Page Info */
$post_object = $wp_query->get_queried_object();
$post_id = $post_object->ID;
$post_title = $post_object->post_title;
$post_excerpt = $post_object->post_excerpt;
$post_content = $post_object->post_content;
$post_thumbnail_url = get_the_post_thumbnail_url($post_id);
?>

<?php
/* Items */
$article = tu_get_article_with_pagination(0, get_query_var('paged'), 6);
$articlePager = paginate_links(array(
    'base' => add_query_arg(array('paged' => '%#%')),
    'format' => '/page/%#%', //'?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $article->max_num_pages,
    'type' => 'list',
    'prev_next' => true,
    'prev_text' => '<i class="fa fa-angle-double-left"></i>',
    'next_text' => '<i class="fa fa-angle-double-right"></i>'
        ));
?>
<div class="container">

    <h2><?php echo $post_title; ?></h2>

    <div class="the-excerpt"><?php echo $post_excerpt; ?></div>

    <div class="the-content"><?php echo $post_content; ?></div>

    <?php if ($article->have_posts()): ?>

        <?php while ($article->have_posts()): $article->the_post(); ?>

            <?php tu_display_each_article(); ?>

            <?php
        endwhile;
        wp_reset_postdata();
        ?>

        <?php echo $articlePager; ?>

    <?php else: ?>
        <p class="text-center" style="margin: 20px 0px;"><?php _e('Nội dung đang được cập nhật..', TEXT_DOMAIN); ?></p>
    <?php endif; ?>

</div>

<?php get_footer(); ?>