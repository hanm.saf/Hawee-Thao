<?php

/**
 * Define constants
 * These constants will be used globally
 */
define('BLOG_NAME', get_option('blogname'));
define('HOME_URL', home_url('/'));
define('TEMPLATE_URL', get_template_directory_uri());
define('TEMPLATE_PATH', get_template_directory());
define('ADMIN_AJAX_URL', admin_url('admin-ajax.php'));
define('IMAGE_URL', TEMPLATE_URL . '/assets/images');
define('TEXT_DOMAIN', 'tu');

/**
 * Constants for configuration
 */
define('FACEBOOK_APP_ID', '165182544028418');
define('FACEBOOK_APP_SECRET', '2d6d9790e17119599bedd1cf725dd85e');

/**
 * Including core stuffs
 */
include_once(TEMPLATE_PATH . '/includes/init.php');
include_once(TEMPLATE_PATH . '/includes/logging.php');
/**
 * Including post-types files
 * You can create more post-types if you need but you should use the structure of existed files
 */
include_once(TEMPLATE_PATH . '/post-types/contact.php');
include_once(TEMPLATE_PATH . '/post-types/article.php');

