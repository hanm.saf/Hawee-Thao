<?php
$message = null;
if (isset($_POST['apply_submit'])) {
    if (isset($_POST['apply_nonce']) && wp_verify_nonce($_POST['apply_nonce'], 'recruitment_apply_form')) {
        $required_fields = array('apply_name', 'apply_email', 'apply_phone', 'apply_content');
        $data = array();
        $data['apply_name'] = isset($_POST['apply_name']) ? sanitize_text_field($_POST['apply_name']) : null;
        $data['apply_email'] = isset($_POST['apply_email']) ? sanitize_text_field($_POST['apply_email']) : null;
        $data['apply_phone'] = isset($_POST['apply_phone']) ? sanitize_text_field($_POST['apply_phone']) : null;
        $data['apply_content'] = isset($_POST['apply_content']) ? sanitize_text_field($_POST['apply_content']) : null;
        
        foreach ($required_fields as $field) {
            if (!isset($data[$field]) || !$data[$field]) {
                $message = __('Vui lòng điền đủ thông tin.', TEXT_DOMAIN);
            }
        }

        if (!is_email($data['apply_email'])) {
            $message = __('Email không hợp lệ.', TEXT_DOMAIN);
        }
        if (!$message) {
            $new_apply_job = array(
                'post_type' => 'cv',
                'post_content' => $_POST['apply_content'],
                'post_title' => sanitize_text_field($_POST['apply_name']) . '-' . sanitize_text_field($_POST['apply_phone']) . '-' . sanitize_text_field($_POST['apply_email']),
                'post_status' => 'pending',
            );

            // Upload CV
            if (isset($_FILES['apply_cv']) && $_FILES['apply_cv']['name'] != "") {
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
                require_once( ABSPATH . 'wp-admin/includes/media.php' );

                $file = $_FILES['apply_cv'];
                $file_ext = pathinfo($file['name'], PATHINFO_EXTENSION);

                if (in_array($file_ext, array('doc', 'docx', 'pdf', 'jpg', 'jpeg', 'png', 'gif')) && $file['error'] == 0) {
                    $new_apply_id = wp_insert_post($new_apply_job);
                    $cv_attach_id = media_handle_upload('apply_cv', $new_apply_id);
                    if (!is_wp_error($cv_attach_id) && !is_wp_error($new_apply_id)) {
                        // Update post meta 
                        add_post_meta($new_apply_id, 'apply_position', (int) $_POST['apply_position']);
                        add_post_meta($new_apply_id, 'apply_name', sanitize_text_field($_POST['apply_name']));
                        add_post_meta($new_apply_id, 'apply_phone', sanitize_text_field($_POST['apply_phone']));
                        add_post_meta($new_apply_id, 'apply_email', sanitize_email($_POST['apply_email']));
                        add_post_meta($new_apply_id, 'apply_cv', $cv_attach_id);

                        $array_mail = array('quthanh95@gmail.com');
                        $headers[] = 'Content-Type: text/html; charset=UTF-8';
                        $content_mail = "Họ tên: " . $_POST['apply_name'] . " --- Số điện thoại: " . $_POST['apply_phone'] . " --- Email : " . $_POST['apply_email'] . " --- Ứng tuyển vị trí: " . get_the_title();

                        foreach ($array_mail as $arr_mail) {
                            wp_mail($arr_mail, "Tuyển dụng", $content_mail);
                        }
                        unset($_POST);
                        $message = __('Cảm ơn bạn đã gửi cv!Chúng tôi sẽ phản hồi bạn sớm nhất!', TEXT_DOMAIN);
                    } else {
                        $message = __('Lỗi gửi CV! Vui lòng thử lại sau ít phút...', TEXT_DOMAIN);
                    }
                } else {
                    $message = __('File không hợp lệ', TEXT_DOMAIN);
                }
            } else {
                $message = __('Vui lòng upload CV của bạn!', TEXT_DOMAIN);
            }
        }
    } else {
        $message = __('Bạn nhập thông tin quá lâu. Vui lòng tải lại trang rồi nhập lại!', TEXT_DOMAIN);
    }
}

get_header();
while (have_posts()):
    the_post();
    $recruitment_id = get_the_ID();
    $recruitment_expiration_date = get_post_meta($recruitment_id, 'recruitment_expiration_date', true);
    $recruitment_workplace = get_post_meta($recruitment_id, 'recruitment_workplace', true);
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $timestamp_recruitment_expiration_date = strtotime($recruitment_expiration_date);
    $date = new DateTime();
    $timestamp_now = $date->getTimestamp();
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8 col-xs-12">
                <h1 class=""><i class="fa fa-caret-right" aria-hidden="true"></i><?php the_title(); ?></h1>
                <div class="">
                    <p>Ngày đăng : <?php echo get_the_time('d.m.Y'); ?><span> Ngày hết hạn : <?php echo str_replace('-', '.', $recruitment_expiration_date); ?></span></p>
                </div>
                <div class="the-content">
                    <?php
                    if (get_the_content() != "") {
                        echo apply_filters('the_content', get_the_content());
                    } else {
                        _e('Nội dung đang được cập nhật!', TEXT_DOMAIN);
                    }
                    ?>
                </div>
                <?php
                if ($timestamp_now < $timestamp_recruitment_expiration_date) {
                    ?>
                    <div>                           
                        <h1><?php _e('Nộp hồ sơ ứng tuyển', TEXT_DOMAIN); ?></h1>
                        <?php if (isset($message) && $message != '') { ?>
                            <div class="alert alert-warning"><?php echo $message; ?></div>
                            <?php
                            $message = null;
                        }
                        ?>
                        <form action="" method="POST" enctype="multipart/form-data">
                            <?php wp_nonce_field('recruitment_apply_form', 'apply_nonce'); ?>
                            <input type="hidden" name="apply_position" value="<?php echo $recruitment_id; ?>">
                            <div class="form-group">
                                <input class="form-control" placeholder="* <?php _e('Tên đầy đủ', TEXT_DOMAIN); ?>" type="text" name="apply_name" id="apply_name" value="<?php echo isset($_POST['apply_name']) ? $_POST['apply_name'] : null; ?>" >
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="* Email" name="apply_email" id="apply_email" value="<?php echo isset($_POST['apply_email']) ? $_POST['apply_email'] : null; ?>" >
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="* <?php _e('Điện thoại', TEXT_DOMAIN); ?>" name="apply_phone" id="apply_phone" value="<?php echo isset($_POST['apply_phone']) ? $_POST['apply_phone'] : null; ?>" >
                            </div>
                            <div class="form-group">
                                <textarea rows="4" class="form-control" placeholder="* <?php _e('Nội dung', TEXT_DOMAIN); ?>" name="apply_content" id="apply_content"><?php echo isset($_POST['apply_content']) ? $_POST['apply_content'] : null; ?></textarea>
                            </div>
                            <div class="form-group btn">

                                <input type="file" class="upload" name="apply_cv" id="apply_cv" >
                            </div>
                            <div class="form-group">
                                <p><i><small>* <?php _e('Vui lòng nhập đầy đủ thông tin', TEXT_DOMAIN); ?></small></i></p>
                            </div>
                            <button type="submit" class="btn" name="apply_submit"><?php _e('Gửi hồ sơ', TEXT_DOMAIN) ?></button>

                        </form>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <h2><?php _e('Các tin khác', TEXT_DOMAIN); ?></h2>
                <ul class="" style="padding-left:0;">
                    <?php
                    $post__not_in = array($recruitment_id);
                    $recruitment_related = tu_get_recruitment_related($post__not_in, 4);
                    if ($recruitment_related->have_posts()):
                        while ($recruitment_related->have_posts()):
                            $recruitment_related->the_post();
                            $recruitment_related_title = get_the_title();
                            ?>
                            <li>
                                <a href="<?php echo get_the_permalink(); ?>"><?php echo $recruitment_related_title; ?></a>
                            </li>
                            <?php
                        endwhile;
                    else:
                        ?>
                        <li>
                            <?php _e('Hiện tại chưa có tin tuyển dụng liên quan nào!', TEXT_DOMAIN); ?>
                        </li>
                    <?php
                    endif;
                    ?>

                </ul>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>

