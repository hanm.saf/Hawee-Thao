<?php get_header(); ?>

<?php
while (have_posts()):
    the_post();
    ?>
    <?php
    $article_id = $post->ID;
    $article_title = get_the_title();
    $article_content = get_the_content();
    $article_link = get_the_permalink();
    $article_thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(), "full");
    ?>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=708262149355322";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div data-rjs="3" style="height:400px;width:400px;background-image: url(<?php echo $article_thumb_url[0]; ?>);background-repeat:no-repeat;background-size:cover;background-position:center center" /></div>

    <div class="container">
        <div class="row">
            <!--Left-->
            <div class="col-md-8 col-sm-9 col-xs-12">
                <h1><?php echo $article_title; ?></h1>
                <!--Social--> 
                <ul class="lsn list-inline">
                    <li><a class="share-popup" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $article_link; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a class="share-popup" href="https://plus.google.com/share?url=<?php echo $article_link; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                </ul>
                <!--End Social--> 
                <!--Content--> 
                <div class="the-content">
                    <?php echo apply_filters('the_content', $article_content); ?>
                </div>
                <!--End content-->
                <!--Fb comment-->
                <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="5"></div>
                <!--End fb comment-->
            </div>
            <!--End left-->
            <!--Right-->
            <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12">
                <h3><?php _e('Bài viết liên quan', TEXT_DOMAIN); ?></h3>
                <ul style="padding-left: 0;">
                    <?php
                    $post__not_in = array($article_id);
                    $article_related = tu_get_article_related(4, $post__not_in);
                    if ($article_related->have_posts()):
                        while ($article_related->have_posts()):
                            $article_related->the_post();
                            $article_related_title = get_the_title();
                            $article_related_link = get_the_permalink();
                            ?>
                            <li><a href="<?php echo $article_related_link; ?>"><?php echo $article_related_title; ?></a></li>                 
                            <?php
                        endwhile;
                    else:
                        ?>
                        <li><?php _e('Hiện tại chưa có bài viết nào!', TEXT_DOMAIN); ?></li>
                    <?php
                    endif;
                    ?>
                </ul>
            </div>
            <!--End right-->
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
              retinajs();
            $('body').delegate('.share-popup', 'click', function (event) {
                event.preventDefault();
                var sharer = $(this).attr('href');
                window.open(sharer, 'sharer', 'toolbar=0,status=0,width=580,height=369');
            });
        });
    </script>
<?php endwhile; ?>
<?php get_footer(); ?>