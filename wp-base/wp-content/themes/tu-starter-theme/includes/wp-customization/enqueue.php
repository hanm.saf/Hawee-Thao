<?php

/**
 * Adding scripts and styles
 * All your scripts and styles will be included in wp_head()
 */
add_action('wp_enqueue_scripts', 'tu_enqueue_scripts_styles');

function tu_enqueue_scripts_styles() {

    if (wp_script_is('media')) {
        wp_enqueue_media();
    }

//    wp_enqueue_style('style', get_stylesheet_uri());
//    Include libary trong sass/libary
    wp_enqueue_style('jquery-ui', TEMPLATE_URL . '/assetsbower_components/query-ui/themes/base/jquery-ui.min.css');
    wp_enqueue_style('bootstrap', TEMPLATE_URL . '/assets/bower_components/bootstrap/dist/css/bootstrap.min.css');
    wp_enqueue_style('fontawesome', TEMPLATE_URL . '/assets/bower_components/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('Swiper', TEMPLATE_URL . '/assets/bower_components/Swiper/dist/css/swiper.min.css');
    wp_enqueue_style('jquery', TEMPLATE_URL . 'assets/bower_components/jquery/jquery.min.css');
    wp_enqueue_style('fancybox', TEMPLATE_URL . '/assets/bower_components/fancybox/dist/jquery.fancybox.min.css');

    wp_enqueue_style('main-style', TEMPLATE_URL . '/assets/stylesheets/sass/main.css', array(), '0.1', false);

    wp_enqueue_script('jquery');
//    Include libary trong addon.js
    wp_enqueue_script('addon', TEMPLATE_URL . '/assets/scripts/addon.js', array(), '0.1', false);
    wp_enqueue_script('bootstrap', TEMPLATE_URL . 'assets\bower_components\bootstrap\dist\js\bootstrap.js', array(), '0.1', false);
    wp_enqueue_script('jquery', TEMPLATE_URL . '/assets/bower_components/jquery/test/jquery.js', array(), '0.1', false);
    wp_enqueue_script('jquery-ui', TEMPLATE_URL . '/assets/bower_components/jquery-ui/jquery-ui.js', array(), '0.1', false);
    wp_enqueue_script('swiper', TEMPLATE_URL . '/assets/bower_components/Swiper/dist/js/swiper.js', array(), '0.1', false);
    wp_enqueue_script('scripts', TEMPLATE_URL . '/assets/scripts/main.js', array(), '0.1', false);
    $wp_script_data = array(
        'ADMIN_AJAX_URL' => ADMIN_AJAX_URL,
        'HOME_URL' => HOME_URL
    );

    wp_localize_script('scripts', 'wp_vars', $wp_script_data);
}




