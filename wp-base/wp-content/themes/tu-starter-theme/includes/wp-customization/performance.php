<?php

//GZIP Trình duyệt
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start('ob_gzhandler');
} else {
    ob_start();
}

/* Async js */

function parsing_of_js($url) {
    if (FALSE === strpos($url, '.js'))
        return $url;
    if (strpos($url, 'jquery.js'))
        return $url;
    return "$url' async onload='";
}

add_filter('clean_url', 'parsing_of_js', 11, 1);

parsing_of_js(ABSPATH . 'wp-includes/js/jquery/jquery-migrate.min.js');
parsing_of_js(TEMPLATE_URL . '/assets/scripts/addon.js');
parsing_of_js(TEMPLATE_URL . '/assets/scripts/main.js');
parsing_of_js(ABSPATH . 'wp-includes/js/wp-embed.min.js');
