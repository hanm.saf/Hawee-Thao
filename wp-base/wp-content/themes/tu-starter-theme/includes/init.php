<?php

/** Constants */
define('HELPERS', TEMPLATE_PATH . '/includes/helpers');
define('LIBRARIES', TEMPLATE_PATH . '/includes/libraries');
define('WP_LIBRARIES', TEMPLATE_PATH . '/includes/wp-libraries');
define('WP_CUSTOMIZATION', TEMPLATE_PATH . '/includes/wp-customization');

/** Helpers */
include_once(HELPERS . '/tu-helpers.php');
include_once(HELPERS . '/wp-helpers.php');

/** Libraries */
include_once(WP_LIBRARIES . '/icon-menu.php');

/** Vendors */
require_once(TEMPLATE_PATH . '/includes/vendor/autoload.php');

/** Wordpress Customization */
include_once(WP_CUSTOMIZATION . '/options.php');
include_once(WP_CUSTOMIZATION . '/admin.php');
include_once(WP_CUSTOMIZATION . '/enqueue.php');
include_once(WP_CUSTOMIZATION . '/languages.php');
include_once(WP_CUSTOMIZATION . '/mail.php');
include_once(WP_CUSTOMIZATION . '/menus.php');
include_once(WP_CUSTOMIZATION . '/security.php');
include_once(WP_CUSTOMIZATION . '/thumbnail-sizes.php');
include_once(WP_CUSTOMIZATION . '/upload.php');
include_once(WP_CUSTOMIZATION . '/widgets.php');
include_once(WP_CUSTOMIZATION . '/editor.php');
include_once(WP_CUSTOMIZATION . '/performance.php');
include_once(WP_CUSTOMIZATION . '/pagination-taxonomy.php');



