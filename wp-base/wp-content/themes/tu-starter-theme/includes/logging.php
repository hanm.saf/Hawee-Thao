<?php
define('LOGGING_PATH', ABSPATH.'wp-log');
date_default_timezone_set('Asia/Ho_Chi_Minh');
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/LoggerInterface.php';
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/AbstractLogger.php';
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/InvalidArgumentException.php';
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/LoggerAwareInterface.php';
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/LoggerAwareTrait.php';

include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/LoggerTrait.php';
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/LogLevel.php';
include_once TEMPLATE_PATH.'/includes/libraries/Psr/Log/NullLogger.php';

include_once TEMPLATE_PATH.'/includes/libraries/KLogger/Logger.php';

add_action('init', 'tu_logging');
function tu_logging() {

    $logger = new Katzgrau\KLogger\Logger(LOGGING_PATH);

    $logInfo = array(
        'current_url' => "//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
        'user_id' => 'Unknown',
        'user_username' => 'Unknown',
        'user_email' => 'Unknown',
        'ip' => tu_get_client_ip_server(),
        'time' => date('Y-m-d H:i'),
        'post' => isset($_POST) ? $_POST : null,
        'get' => isset($_GET) ? $_GET : null,
        '---' => '---'
    );

    if (is_user_logged_in()) {
        $currentUser = wp_get_current_user();
        $logInfo['user_id'] = $currentUser->ID;
        $logInfo['user_username'] = $currentUser->user_login;
        $logInfo['user_email'] = $currentUser->user_email;
    }

    $logger->info('----------------------------------------------------------'.date('Y-m-d H:i'), $logInfo);
}

function tu_get_client_ip_server() {
    global $_SERVER;

    if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}