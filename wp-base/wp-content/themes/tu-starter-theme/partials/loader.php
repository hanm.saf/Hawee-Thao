<?php
/**
 * NOTE:
 * Loader is separated to all others pages.
 * Styles, scripts are independent with the enqueue to optimize site speed.
 */
?>
<div class="sk-fading-circle" id="tu_loader">
    <div class="text">Loading</div>
    <div class="sk-circle1 sk-circle"></div>
    <div class="sk-circle2 sk-circle"></div>
    <div class="sk-circle3 sk-circle"></div>
    <div class="sk-circle4 sk-circle"></div>
    <div class="sk-circle5 sk-circle"></div>
    <div class="sk-circle6 sk-circle"></div>
    <div class="sk-circle7 sk-circle"></div>
    <div class="sk-circle8 sk-circle"></div>
    <div class="sk-circle9 sk-circle"></div>
    <div class="sk-circle10 sk-circle"></div>
    <div class="sk-circle11 sk-circle"></div>
    <div class="sk-circle12 sk-circle"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(window).load(function () {
            $("#tu_loader").fadeOut("slow");
        });
    });
</script>