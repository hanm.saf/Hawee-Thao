    <div class="_1header">
        <div class="container">
            <div class="logoleft"><img src="<?php echo IMAGE_URL.'../../images/logo1.png'; ?>"></div>

            <div class="logoright"><img src="<?php echo IMAGE_URL.'../../images/logo2.png'; ?>"></div>
            <div class="menu">
                <nav class="navbar navbar-light">
                    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#mynav">
                    </button>
                    <div class="collapse navbar-toggleable-xs" id="mynav">
                        <ul class="nav navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">trang chủ<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">giới thiệu</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">vị trí</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">360 view</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">sản phẩm</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">tin tức</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">liên hệ</a>
                            </li>
                        </ul>
                    </div>
                </nav>              
            </div>
            <div class="contact">
                <div class="language">
                    <span class="nau">vi</span> 
                    <div class="stick"></div>
                    <span class="den">en</span>
                </div>
                <div class="hotline">
                    <span class="den">hotline:</span>
                    <span class="nau">094 111 998</span>
                </div>
            </div>
        </div>
    </div>