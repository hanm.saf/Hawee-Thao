<?php
get_header();
?>
<main>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/header.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/banner.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/resedence.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/goldplace.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/panorama.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/design.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/real.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/register.php'); ?>
	<?php include_once(TEMPLATE_PATH.'/partials/forms/footer.php'); ?>
</main>
<?php get_footer(); ?>