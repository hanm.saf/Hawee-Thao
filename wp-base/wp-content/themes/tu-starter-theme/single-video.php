<?php
get_header();
?>

<?php
/* Current post info */
$post_object = $wp_query->get_queried_object();
$post_id = $post_object->ID;
$post_title = $post_object->post_title;
$post_excerpt = $post_object->post_excerpt;
$post_content = $post_object->post_content;
$post_thumbnail_url = get_the_post_thumbnail_url($post_id, 'full');
$post_youtube_url = get_post_meta($post_id, 'video_youtube_url', true);
?>

    <div class="container video-single">

        <h2><?php echo $post_title; ?></h2>

        <div class="the-excerpt"><?php echo $post_excerpt; ?></div>

        <div class="the-content"><?php echo $post_content; ?></div>

        <div id="video_player" class="video-player-thumb" style="background-image: url('<?php echo $post_thumbnail_url; ?>');"></div>

        <script>
            jQuery(document).ready(function($){
                $('#video_player').click(function(evt){
                    $(this).html('<div class="youtube-player"><iframe src="//www.youtube.com/embed/<?php echo $post_youtube_url; ?>?autoplay=1&controls=0&showinfo=0" frameborder="0" allowfullscreen></iframe></div>');
                    $(this).addClass('playing');
                });
            });
        </script>


    </div>

<?php get_footer(); ?>