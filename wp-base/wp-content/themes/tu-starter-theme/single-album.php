<?php get_header(); ?>

<?php
while (have_posts()):
    the_post();
    ?>
    <?php
    $album_id = $post->ID;
    $album_title = get_the_title();
    $album_content = get_the_content();
    $album_images = get_post_meta($album_id, 'album_images', true);
    ?>
    <div class="container">
        <div class="row">
            <!--Left-->
            <div class="col-md-8 col-sm-9 col-xs-12">
                <h1><?php echo $album_title; ?></h1>
                <!--Content--> 
                <div class="the-content">
                    <?php echo apply_filters('the_content', $album_content); ?>
                </div>
                <!--End content-->
                <!-- Slide -->
                <div class="slide">
                    <div class="swiper-container" id="swiper_album">
                        <div class="swiper-wrapper">
                            <?php
                            foreach ($album_images as $image):
                                $image_thumb_url = wp_get_attachment_image_src($image['id'], 'thumb-album');
                                $image_title = $image['title'];
                                $image_external_url = $image['external_url'];
                                $image_description = $image['description'];
                                ?>
                                <div class="swiper-slide item">
                                    <div class="image">
                                        <a href="<?php echo $image_external_url; ?>"><img src="<?php echo $image_thumb_url[0] ? $image_thumb_url[0] : NO_IMAGE_URL; ?>" /></a>
                                    </div>
                                    <div class="text">
                                        <h3 class="title"><a href="<?php echo $image_external_url; ?>"><?php echo $image_title; ?></a></h3>
                                        <div class="excerpt">
                                            <?php echo apply_filters('the_content', $image_description); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                <!-- End slide -->
            </div>
            <!--End left-->
            <!--Right-->
            <div class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12">
                <h3><?php _e('Hình ảnh liên quan', TEXT_DOMAIN); ?></h3>
                <ul style="padding-left: 0;">
                    <?php
                    $post__not_in = array($album_id);
                    $album_related = tu_get_album_related(4, $post__not_in);
                    if ($album_related->have_posts()):
                        while ($album_related->have_posts()):
                            $album_related->the_post();
                            $album_related_title = get_the_title();
                            $album_related_link = get_the_permalink();
                            ?>
                            <li><a href="<?php echo $album_related_link; ?>"><?php echo $album_related_title; ?></a></li>                 
                            <?php
                        endwhile;
                    else:
                        ?>
                        <li><?php _e('Hiện tại chưa có album nào!', TEXT_DOMAIN); ?></li>
                    <?php
                    endif;
                    ?>
                </ul>
            </div>
            <!--End right-->
        </div>
    </div>
    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('#swiper_album', {
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 30,
            autoHeight: true
        });
    </script>
<?php endwhile; ?>
<?php get_footer(); ?>