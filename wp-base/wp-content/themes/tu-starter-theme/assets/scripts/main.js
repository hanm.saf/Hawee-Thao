jQuery(document).ready(function ($) {
//        update class table rps
    $('table').each(function () {
        var element = $(this);
        // Create the wrapper element
        var scrollWrapper = $('<div />', {
            'class': 'table-responsive',
            'html': '<div />' // The inner div is needed for styling
        }).insertBefore(element);
        // Store a reference to the wrapper element
        element.data('scrollWrapper', scrollWrapper);
        // Move the scrollable element inside the wrapper element
        element.appendTo(scrollWrapper.find('div'));
    });

//    remove space p
    $('p').filter(function () {
        return $.trim($(this).text()) === '' && $(this).children().length == 0
    }).remove();
    
});