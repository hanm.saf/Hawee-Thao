<?php
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_subscriber');

function reg_post_type_subscriber() {
    //Change this when creating post type
    $post_type_name = __('Đăng ký', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-feedback',
        'supports' => array('title', 'editor'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false,
         'has_archive' => false,
        'capabilities' => array(
            'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
        ),
        'map_meta_cap' => true
    );
    register_post_type('subscriber', $args);
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_subscriber');

function add_metabox_subscriber() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_subscriber_general($post) {
        $post_id = $post->ID;
        $subscriber_email = get_post_meta($post_id, 'subscriber_email', true);
        $subscriber_name = get_post_meta($post_id, 'subscriber_name', true);
        $subscriber_phone = get_post_meta($post_id, 'subscriber_phone', true);
        $subscriber_company = get_post_meta($post_id, 'subscriber_company', true);
        $subscriber_question = get_post_meta($post_id, 'subscriber_question', true);
        $subscriber_position = get_post_meta($post_id, 'subscriber_position', true);
        $subscriber_document_is_downloaded = get_post_meta($post_id, 'subscriber_document_is_downloaded', true);
        $subscriber_document_downloaded_time = get_post_meta($post_id, 'subscriber_document_downloaded_time', true);
        $subscriber_document_is_downloaded_label = $subscriber_document_is_downloaded == true ? 'Đã tải xuống lúc ' . date('H:i d/m/Y', $subscriber_document_downloaded_time) : 'Chưa tải xuống';
        $subscriber_sent_mail = get_post_meta($post_id, 'subscriber_sent_mail', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_subscriber'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="">Họ tên</label></th>
                    <td><?php echo $subscriber_name; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Email</label></th>
                    <td><?php echo $subscriber_email; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">SĐT</label></th>
                    <td><?php echo $subscriber_phone; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Đơn vị công tác</label></th>
                    <td><?php echo $subscriber_company; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Chức vụ</label></th>
                    <td><?php echo $subscriber_position; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Nội dung</label></th>
                    <td><?php echo $subscriber_question; ?></td>
                </tr>

                <tr>
                    <th scope="row"><label for="">Đã gửi mail?</label></th>
                    <td>
        <?php echo $subscriber_sent_mail == true ? 'Đã gửi' : 'Lỗi'; ?>
                        &nbsp;&nbsp;<button class="button button-default button-small" style="vertical-align: middle;" type="submit" name="subscriber_resend_email" value="true">Gửi lại</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Đã tải tài liệu xuống?</label></th>
                    <td><?php echo $subscriber_document_is_downloaded_label; ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_subscriber_general', 'Thông tin cơ bản', 'display_metabox_subscriber_general', 'subscriber', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_subscriber');

function save_metabox_subscriber($post_id) {
    if (get_post_type() == 'subscriber' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_subscriber')) {

        if (isset($_POST['subscriber_resend_email']) && $_POST['subscriber_resend_email'] == true) {

            subscriber_send_mail_with_download_url($post_id);
        }
    }
}

/**
 * AJAX
 */
add_action('wp_ajax_subscriber_submit_ajax', 'subscriber_submit_ajax');
add_action('wp_ajax_nopriv_subscriber_submit_ajax', 'subscriber_submit_ajax');

function subscriber_submit_ajax() {

    if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'subscriber_submit_ajax')) {
        echo json_encode(array('success' => false, 'msg' => 'Phiên làm việc đã hết, vui lòng tải lại trang và thử lại.'));
        exit;
    }

    $data = array();

    if (isset($_POST['subscriber_gender'])) {
        $data['subscriber_gender'] = (int) $_POST['subscriber_gender'];

        if ($data['subscriber_gender'] == 2) {
            $data['subscriber_gender_label'] = 'Bà';
        } else {
            $data['subscriber_gender_label'] = 'Ông';
        }
    }

    if (isset($_POST['subscriber_name'])) {
        $data['subscriber_name'] = sanitize_text_field($_POST['subscriber_name']);
    }

    if (isset($_POST['subscriber_company'])) {
        $data['subscriber_company'] = sanitize_text_field($_POST['subscriber_company']);
    }

    if (isset($_POST['subscriber_position'])) {
        $data['subscriber_position'] = sanitize_text_field($_POST['subscriber_position']);
    }

    if (isset($_POST['subscriber_phone'])) {
        $data['subscriber_phone'] = sanitize_text_field($_POST['subscriber_phone']);
    }

    if (isset($_POST['subscriber_email'])) {
        $data['subscriber_email'] = sanitize_text_field($_POST['subscriber_email']);
    }

    foreach ($data as $d) {
        if (!$d) {
            echo json_encode(array('success' => false, 'msg' => 'Vui lòng điền đầy đủ thông tin.'));
            exit;
        }
    }

    if (!is_email($data['subscriber_email'])) {
        echo json_encode(array('success' => false, 'msg' => 'Email không hợp lệ.'));
        exit;
    }

    $data['subscriber_question'] = sanitize_text_field($_POST['subscriber_question']);

    $subscriber_post = array(
        'post_title' => $data['subscriber_gender_label'] . ' ' . $data['subscriber_name'] . ' - ' . $data['subscriber_email'] . ' - ' . $data['subscriber_phone'] . ' (' . date('Y-m-d') . ')',
        'post_content' => '',
        'post_status' => 'pending',
        'post_type' => 'subscriber'
    );

    $new_subscriber_user = wp_insert_post($subscriber_post);

    if (!is_wp_error($new_subscriber_user)) {

        update_post_meta($new_subscriber_user, 'subscriber_name', $data['subscriber_name']);
        update_post_meta($new_subscriber_user, 'subscriber_email', $data['subscriber_email']);
        update_post_meta($new_subscriber_user, 'subscriber_phone', $data['subscriber_phone']);
        update_post_meta($new_subscriber_user, 'subscriber_company', $data['subscriber_company']);
        update_post_meta($new_subscriber_user, 'subscriber_position', $data['subscriber_position']);
        update_post_meta($new_subscriber_user, 'subscriber_question', $data['subscriber_question']);
        update_post_meta($new_subscriber_user, 'subscriber_gender', $data['subscriber_gender']);
        update_post_meta($new_subscriber_user, 'subscriber_gender_label', $data['subscriber_gender_label']);

        subscriber_send_mail_with_download_url($new_subscriber_user, $data);

        echo json_encode(array(
            'success' => true,
            'msg' => 'Thông tin đăng ký đã được gửi đi, chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất.',
            'email' => $data['subscriber_email']
        ));
        exit;
    } else {
        echo json_encode(array('success' => false, 'msg' => 'Có lỗi xảy ra vui lòng thử lại.'));
        exit;
    }

    exit;
}

/**
 * EMAIL
 */
function subscriber_send_mail_with_download_url($subscriberId, $data = null) {
    if (!$subscriberId) {
        return false;
    }

    if (!$data) {

        $subscriber_email = get_post_meta($subscriberId, 'subscriber_email', true);
        $subscriber_name = get_post_meta($subscriberId, 'subscriber_name', true);

        $data = array(
            'subscriber_email' => $subscriber_email,
            'subscriber_name' => $subscriber_name
        );
    }

    $downloadUrl = subscriber_get_download_url_for_subscriber($subscriberId);

    $emailTemplate = '<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Download bộ tài liệu Digital Marketing 2016</title>
</head>
<body style="margin: 0; padding: 0;">
	<div style="background-color: #F2F2F2; padding: 20px 0; margin: 0; font-family: Arial, sans-serif;">
		<div style="width: 90%; max-width:600px; margin: 0 auto; padding: 1% 2%; background: #FFF;">
			<p style="text-align: center"><img src="' . IMAGE_URL . '/key-visual.png" width="100%" border="0" alt="" /></p>
			<p>Thân gửi {{name}},</p>
			<p>Cảm ơn bạn đã đăng ký nhận bộ tài liệu "<b>Digital Marketing 2016 dành cho thị trường Việt Nam</b>" do Time Universal biên soạn. Vui lòng tải trực tiếp trọn bộ tài liệu bằng cách click vào nút "download" dưới đây:</p>
			<p>&nbsp;</p>
			<p><a href="{{download_url}}" style="background-color:#D0232E; padding:10px; display:block; margin:0px auto; width:180px; text-align:center; color:#FFF; font-size:25px; text-decoration:none;">DOWNLOAD</a></p>
			<p>&nbsp;</p>
			<p>Nếu gặp bất kỳ trục trặc nào trong quá trình tải tài liệu, xin vui lòng email về hòm thư: <a href="mailto:info@timevn.com">info@timevn.com</a> để nhận được hỗ trợ trực tiếp từ chúng tôi.</p>
			<p>Để tham khảo những tài liệu và thông tin chuyên môn khác về lĩnh vực Digital Marketing, bạn có thể truy cập: <a href="http://blog.timeuniversal.vn">http://blog.timeuniversal.vn</a> hoặc follow chúng tôi tại <a href="https://www.facebook.com/timeuniversal">Facebook</a> hoặc <a href="http://timeuniversal.vn">Website.</a></p>
			<p>&nbsp;</p>
			<p>Trân trọng,</p>
			<p>Time Universal</p>
		</div>
	</div>
</body>
</html>';

    $email = array(
        'to' => $data['subscriber_email'],
        'subject' => '[Time Universal] Download bộ tài liệu Digital Marketing 2016',
        'headers' => 'From: Time Universal',
        'message' => ''
    );

    $email['message'] = str_replace('{{name}}', $data['subscriber_name'], $emailTemplate);
    $email['message'] = str_replace('{{download_url}}', $downloadUrl, $email['message']);

    $result = wp_mail($email['to'], $email['subject'], $email['message'], $email['headers']);

    if ($result == true) {
        update_post_meta($subscriberId, 'subscriber_sent_mail', true);
    } else {
        update_post_meta($subscriberId, 'subscriber_sent_mail', false);
    }

    return $result;
}

function subscriber_get_download_url_for_subscriber($subscriberId) {
    $downloadCode = tu_encode($subscriberId, 'vbm2016');
    return tu_get_permalink_by_slug('download', 'page') . '?code=' . $downloadCode;
}

function subscriber_download_file_by_code($downloadCode = '') {
    if ($downloadCode) {

        $subscriberId = tu_decode($downloadCode, 'vbm2016');

        if ($subscriberId) {

            $subscriber = get_post($subscriberId);

            if ($subscriber) {

                update_post_meta($subscriberId, 'subscriber_document_is_downloaded', true);
                update_post_meta($subscriberId, 'subscriber_document_downloaded_time', time());

                $upload_dir = wp_upload_dir();

                $fileURL = realpath($upload_dir['basedir'] . '/Full4Files-LtL0HGJby9rSDGympIbuOBxhy.zip');

                if ($fileURL) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename=Digital-Marketing-2016-danh-cho-thi-truong-Viet-Nam.zip');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($fileURL));
                    ob_clean();
                    flush();
                    readfile($fileURL);
                    exit;
                }
            }
        }
    }

    header("location: " . get_permalink(2));
    exit;
}

/**
 * NOTIFICATION
 */
add_action('wp_before_admin_bar_render', 'notification_subscriber_admin_bar');

function notification_subscriber_admin_bar() {

    global $wp_admin_bar;

    $subscribers = get_posts(array('post_type' => 'subscriber', 'posts_per_page' => -1, 'post_status' => 'pending'));

    if (count($subscribers) > 0) {

        $wp_admin_bar->add_node(array(
            'id' => 'has-subscriber-email',
            'title' => '<span class="ab-icon"></span> <span class="ab-label pending-count count-1">' . count($subscribers) . '</span>',
            'href' => admin_url('edit.php?post_status=pending&post_type=subscriber'),
            'meta' => array(
                'title' => 'Bạn có ' . count($subscribers) . ' thư chưa trả lời'
            )
        ));
    }
    ?>

    <?php
}

add_action('admin_menu', 'notification_bubble_in_admin_menu_contact');

function notification_bubble_in_admin_menu_contact() {
    global $menu;
    $contacts = get_posts(array('post_type' => 'subscriber', 'posts_per_page' => -1, 'post_status' => 'pending'));
    $menu[6][0] .= $contacts ? "&nbsp;<span class='update-plugins count-1' title='Bạn có " . count($contacts) . " thư chưa trả lời'><span class='update-count'>" . count($contacts) . "</span></span>" : '';
}
