<?php
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_product');

function reg_post_type_product() {
    //Change this when creating post type
    $post_type_name = __('Sản phẩm', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-screenoptions',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );
    register_post_type('product', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('product_category', 'product', array(
        "hierarchical" => true,
        "label" => __('Danh mục sản phẩm', 'tu'),
        "singular_label" => __('Danh mục', 'tu'),
        "rewrite" => array('slug' => __('danh-muc-san-pham', 'tu'), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * RETRIEVING FUNCTIONS
 */

/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_product_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'product',
        'order' => 'DESC',
        'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['product_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'product_category',
            'field' => 'id',
            'terms' => $options['product_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * Display each product with class names
 * @param string $class
 */
function tu_display_each_product($class = "row item") {
    $post_id = get_the_ID();
    $permalink = get_permalink($post_id);
    $categories = tu_get_post_terms($post_id, 'product_category', true, 'itemprop="genre" class="item-category"');
    $post_date = date('d/m/Y', strtotime(get_the_date()));
    $excerpt = wp_trim_words(get_the_excerpt(), 25);
    ?>
    <li class="<?php echo $class; ?>">
        <div class="col-md-4 col-sm-12 col-xs-12">
    <?php echo get_the_post_thumbnail($post_id, 'thumbnail-product'); ?>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12">
            <h3 class="item-title"><a href="<?php echo $permalink; ?>"><?php echo get_the_title(); ?></a></h3>

            <p class="item-categories"><?php echo $categories; ?></p>

            <p class="item-date"><?php echo $post_date; ?></p>

            <p class="item-description"><?php echo $excerpt; ?></p>
        </div>
    </li>
    <?php
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_product');

function add_metabox_product() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_product_general($post) {
        $post_id = $post->ID;
        $product_is_hot = get_post_meta($post_id, 'product_is_hot', true);
        $product_is_new = get_post_meta($post_id, 'product_is_new', true);
        $product_price = (int) get_post_meta($post_id, 'product_price', true);
        $product_price_sale_off = (int) get_post_meta($post_id, 'product_price_sale_off', true);
        $product_price_sale_off = $product_price_sale_off == 0 ? $product_price : $product_price_sale_off;
        $product_sku = get_post_meta($post_id, 'product_sku', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_product'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="product_is_hot"><?php _e('Sản phẩm nổi bật', 'tu') ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="product_is_hot" name="product_is_hot"
                               value="1" <?php if ($product_is_hot == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="product_is_new"><?php _e('Sản phẩm mới', 'tu') ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="product_is_new" name="product_is_new"
                               value="1" <?php if ($product_is_new == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="product_sku"><?php _e('Mã sản phẩm', 'tu') ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="text" class="regular-text" id="product_sku" name="product_sku" value="<?php echo $product_sku; ?>"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="product_price"><?php _e('Giá bán', 'tu') ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="text" class="regular-text" id="product_price" name="product_price" value="<?php echo $product_price; ?>"/>
                        (VNĐ)
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="product_price_sale_off"><?php _e('Giá khuyến mãi (Nếu có)', 'tu') ?></label>
                    </th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="text" class="regular-text" id="product_price_sale_off" name="product_price_sale_off"
                               value="<?php echo $product_price_sale_off; ?>"/> (VNĐ)
                        <p class="description">
                            (Giảm <?php if ($product_price > 0) echo tu_get_percent($product_price, $product_price_sale_off);
        else echo '0%'; ?>
                            )
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_product_general', 'Thông tin cơ bản', 'display_metabox_product_general', 'product', 'normal', 'high'
    );

    /**
     * Meta box for images
     * @param $post
     */
    function display_metabox_product_images($post) {
        $post_id = $post->ID;
        $product_images_ids = get_post_meta($post_id, 'product_images_ids', true);
        ?>
        <table class="form-table">
            <tbody>
                <tr class="upload-row">
                    <th scope="row">
                        <button type="button" class="button upload-button">Chọn ảnh</button>
                        <input type="hidden" class="upload-files-ids" name="product_images_ids[0]"
                               value="<?php echo $product_images_ids; ?>">
                    </th>
                    <td>
                        <div
                            class="upload-files-preview"><?php echo tu_get_images_html_by_attachment_ids($product_images_ids); ?></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                //Multiple Images Upload
                jQuery('.upload-row').delegate('.upload-button', 'click', function (event) {
                    var _this = jQuery(this);

                    var file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select image',
                        library: {},
                        button: {text: 'Select'},
                        //frame: 'post',
                        multiple: true
                    });

                    file_frame.on('select', function () {
                        var attachment_ids = [];
                        attachment = file_frame.state().get('selection').toJSON();
                        console.log(attachment);

                        imgs_html = '';
                        jQuery.each(attachment, function (index, item) {
                            attachment_ids.push(item.id);
                            imgs_html += '<img src="' + item.url + '" />';
                        });

                        var _this_parent = _this.parents('.upload-row');
                        _this_parent.find('.upload-files-ids').val(attachment_ids.join(','));
                        _this_parent.find('.upload-files-preview').html(imgs_html);
                    });

                    file_frame.open();
                });

                //Sortable
                jQuery('.upload-row').find('.upload-files-preview').sortable({
                    start: function (e, ui) {

                    },
                    update: function (e, ui) {
                        var _this_parent = jQuery(this).parents('.upload-row');
                        var new_attachment_ids = [];
                        jQuery.each(_this_parent.find('.upload-files-preview img'), function (index, item) {
                            var this_img_id = jQuery(this).attr("data-img-id");
                            new_attachment_ids.push(this_img_id);
                        });
                        _this_parent.find('.upload-files-ids').val(new_attachment_ids.join(','));
                    }
                });
                jQuery('.upload-row').find('.upload-files-preview').disableSelection();

            });
        </script>
        <style type="text/css">
            .upload-files-preview img {
                width: 24%;
                margin-right: 1%;
            }
        </style>
        <?php
    }

    add_meta_box(
            'display_metabox_product_images', 'Ảnh sản phẩm', 'display_metabox_product_images', 'product', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_product');

function save_metabox_product($post_id) {
    if (get_post_type() == 'product' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_product')) {

        if (isset($_POST['product_is_hot'])) {
            $product_is_hot = (int) $_POST['product_is_hot'] ? 'true' : 'false';
            update_post_meta($post_id, 'product_is_hot', $product_is_hot);
        }

        if (isset($_POST['product_is_new'])) {
            $product_is_new = (int) $_POST['product_is_new'] ? 'true' : 'false';
            update_post_meta($post_id, 'product_is_new', $product_is_new);
        }

        if (isset($_POST['product_price'])) {
            $product_price = (int) $_POST['product_price'];
            if ($product_price > 0) {
                $product_price_sale_off = (int) $_POST['product_price_sale_off'];
                $product_price_sale_off = ($product_price_sale_off <= 0 || $product_price_sale_off > $product_price) ? $product_price : $product_price_sale_off;
                $product_is_sale_off = $product_price_sale_off != $product_price ? 'true' : 'false';

                update_post_meta($post_id, 'product_price', $product_price);
                update_post_meta($post_id, 'product_price_sale_off', $product_price_sale_off);
                update_post_meta($post_id, 'product_is_sale_off', $product_is_sale_off);
            }
        }

        if (isset($_POST['product_sku'])) {
            update_post_meta($post_id, 'product_sku', sanitize_text_field($_POST['product_sku']));
        }

        if (isset($_POST['product_images_ids'])) {
            update_post_meta($post_id, 'product_images_ids', sanitize_text_field($_POST['product_images_ids'][0]));
        }
    }
}

/**
 * AJAX HANDLES
 */
/**
 * Get image src by id
 */
add_action('wp_ajax_get_image_src_ajax', 'get_image_src_ajax');
add_action('wp_ajax_nopriv_get_image_src_ajax', 'get_image_src_ajax');

function get_image_src_ajax() {
    echo tu_get_image_src((int) $_POST['id']);
    exit;
}
