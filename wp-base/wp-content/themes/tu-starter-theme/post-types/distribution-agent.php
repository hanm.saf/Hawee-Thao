<?php
/**
 * Author: 
 * Date: 08/02/2017
 * Note: 
 */
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_distribution_agent');

function reg_post_type_distribution_agent() {
    //Change this when creating post type
    $post_type_name = __('Đại lý phân phối', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-networking',
        'supports' => array('title', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('distribution_agent', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('distribution_agent_category', 'distribution_agent', array(
        "public" => true,
        "hierarchical" => true,
        "label" => __('Danh mục đại lý phân phối', TEXT_DOMAIN),
        "singular_label" => __('Danh mục', TEXT_DOMAIN),
        "rewrite" => false,
        "show_admin_column" => true
    ));
}

/**
 * Getting distribution_agents with Pagination
 * @param int $category_id
 * @param int $page
 * @param int $post_per_page
 * @return WP_Query
 */
function tu_get_distribution_agent_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'distribution_agent',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['distribution_agent_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'distribution_agent_category',
            'field' => 'id',
            'terms' => $options['distribution_agent_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_distribution_agent');

function add_metabox_distribution_agent() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_distribution_agent_general($post) {
        $post_id = $post->ID;
        $distribution_agent_address = get_post_meta($post_id, 'distribution_agent_address', true);
        $distribution_agent_phone = get_post_meta($post_id, 'distribution_agent_phone', true);
        $distribution_agent_website = get_post_meta($post_id, 'distribution_agent_website', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_distribution_agent'); ?>">
            <input type="hidden" name="do" value="post"/>
            <tbody>
                <tr>
                    <th scope="row"><label for="distribution_agent_address"><?php _e('Địa chỉ', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="text" id="distribution_agent_address" class="regular-text" name="distribution_agent_address" value="<?php echo $distribution_agent_address; ?>"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="distribution_agent_phone"><?php _e('SĐT', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="text" id="distribution_agent_phone" class="regular-text" name="distribution_agent_phone" value="<?php echo $distribution_agent_phone; ?>"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="distribution_agent_website"><?php _e('Website', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="text" id="distribution_agent_website" class="regular-text" name="distribution_agent_website" value="<?php echo $distribution_agent_website; ?>"/>
                    </td>
                </tr>

            </tbody>
        </table>

        <?php
    }

    add_meta_box(
            'display_metabox_distribution_agent_general', 'Thông tin cơ bản', 'display_metabox_distribution_agent_general', 'distribution_agent', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_distribution_agent');

function save_metabox_distribution_agent($post_id) {
    if (get_post_type() == 'distribution_agent' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_distribution_agent')) {

        if (isset($_POST['distribution_agent_address'])) {
            update_post_meta($post_id, 'distribution_agent_address', sanitize_text_field($_POST['distribution_agent_address']));
        }

        if (isset($_POST['distribution_agent_phone'])) {
            update_post_meta($post_id, 'distribution_agent_phone', sanitize_text_field($_POST['distribution_agent_phone']));
        }

        if (isset($_POST['distribution_agent_website'])) {
            update_post_meta($post_id, 'distribution_agent_website', sanitize_text_field($_POST['distribution_agent_website']));
        }
    }
}
