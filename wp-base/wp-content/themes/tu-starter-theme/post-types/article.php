<?php
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_article');

function reg_post_type_article() {
    //Change this when creating post type
    $post_type_name = __('Bài viết', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('article', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('article_category', array('article'), array(
        "hierarchical" => true,
        "label" => __('Chuyên mục', TEXT_DOMAIN),
        "singular_label" => __('Chuyên mục', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('chuyen-muc', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * RETRIEVING FUNCTIONS
 */

/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_article_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'article',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['article_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'article_category',
            'field' => 'id',
            'terms' => $options['article_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $post__not_in
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_article_related($page = 1, $post_per_page = 10, $post__not_in = array(), $options = array()) {
    $args = array(
        'post_type' => 'article',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post__not_in' => $post__not_in,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['article_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'article_category',
            'field' => 'id',
            'terms' => $options['article_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * Display each article with class names
 * @param string $class
 */
function tu_display_each_article($class = "row item") {
    $post_id = get_the_ID();
    $permalink = get_permalink($post_id);
    $categories = tu_get_post_terms($post_id, 'article_category', true, 'itemprop="genre" class="item-category"');
    $post_date = get_the_date('d/m/Y');
    $excerpt = wp_trim_words(get_the_excerpt(), 25);
    ?>
    <li class="<?php echo $class; ?>">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <?php echo get_the_post_thumbnail($post_id, 'thumbnail'); ?>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12">

            <h3 class="item-title"><a href="<?php echo $permalink; ?>"><?php echo get_the_title(); ?></a></h3>

            <p class="item-categories"><?php echo $categories; ?></p>

            <p class="item-date"><?php echo $post_date; ?></p>

            <p class="item-description"><?php echo $excerpt; ?></p>
        </div>
    </li>
    <?php
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_article');

function add_metabox_article() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_article_general($post) {
        $post_id = $post->ID;
        $article_is_hot = get_post_meta($post_id, 'article_is_hot', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_article'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="article_is_hot"><?php _e('Bài viết nổi bật', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="article_is_hot" name="article_is_hot"
                               value="1" <?php if ($article_is_hot == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_article_general', 'Thông tin cơ bản', 'display_metabox_article_general', 'article', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_article');

function save_metabox_article($post_id) {
    if (get_post_type() == 'article' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_article')) {

        $article_is_hot = isset($_POST['article_is_hot']) && (int) $_POST['article_is_hot'] ? 'true' : 'false';
        update_post_meta($post_id, 'article_is_hot', $article_is_hot);
    }
}

/**
 * ADDING TERMS META BOXES
 */
/**
 * Term meta box article_category
 */
add_action('article_category_edit_form_fields', 'display_metabox_article_category', 99, 2);

function display_metabox_article_category($term) {
    if ($term->parent == 0):
        $article_category_image_id = get_term_meta($term->term_id, 'article_category_image_id', true);
        $article_category_title = get_term_meta($term->term_id, 'article_category_title', true);
        $article_category_color = get_term_meta($term->term_id, 'article_category_color', true);
        ?>
        <table id="ads" class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="article_category_title"><?php _e('Tiêu đề', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="text" id="article_category_title" class="regular-text" name="article_category_title" value="<?php echo $article_category_title; ?>"/>
                    </td>
                </tr>
                <tr class="upload-row">
                    <th scope="row">
                        <button type="button" class="button upload-button">Chọn icon (565x276px)</button>
                        <input type="hidden" class="upload-files-ids" name="article_category_image_id" value="<?php echo $article_category_image_id; ?>">
                    </th>
                    <td>
                        <div
                            class="upload-files-preview"><?php echo tu_get_images_html_by_attachment_ids($article_category_image_id); ?></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="article_category_color"><?php _e('Mã màu đại diện', TEXT_DOMAIN) ?> &nbsp;<span style="display: inline-block; width: 20px; height: 20px; background: <?php echo $article_category_color ? : '#FFF'; ?>"></span></label></th>
                    <td>
                        <input type="text" id="article_category_color" class="regular-text" name="article_category_color" value="<?php echo $article_category_color; ?>"/>
                    </td>
                </tr>
            </tbody>
        </table>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                //Single Image Upload
                jQuery('.upload-row').delegate('.upload-button', 'click', function (event) {
                    var _this = jQuery(this);

                    var file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select image',
                        library: {},
                        button: {text: 'Select'},
                        //frame: 'post',
                        multiple: false
                    });

                    file_frame.on('select', function () {
                        var attachment = file_frame.state().get('selection').first().toJSON();
                        var _this_parent = _this.parents('.upload-row');
                        _this_parent.find('.upload-files-ids').val(attachment.id);
                        _this_parent.find('.upload-files-preview').html('<img width="565px" src="' + attachment.url + '" />');
                    });

                    file_frame.open();
                });
            });
        </script>

        <style type="text/css">
            .upload-files-preview img {
                max-width: 100%;
                margin-right: 1%;
            }
        </style>
        <?php
    endif;
}

add_action('create_article_category', 'save_metabox_article_category', 10, 2);
add_action('edited_article_category', 'save_metabox_article_category', 10, 2);

function save_metabox_article_category($term_id) {

    if (isset($_POST['article_category_image_id'])) {
        update_term_meta($term_id, 'article_category_image_id', $_POST['article_category_image_id']);
    }
    if (isset($_POST['article_category_title'])) {
        update_term_meta($term_id, 'article_category_title', $_POST['article_category_title']);
    }
    if (isset($_POST['article_category_color'])) {
        update_term_meta($term_id, 'article_category_color', $_POST['article_category_color']);
    }
}
