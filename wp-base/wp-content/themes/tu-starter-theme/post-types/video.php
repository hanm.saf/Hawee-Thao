<?php
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_video');

function reg_post_type_video() {
    //Change this when creating post type
    $post_type_name = __('Video', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-format-video',
        'supports' => array('title', 'thumbnail', 'excerpt', 'editor'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('video', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('video_category', 'video', array(
        "hierarchical" => true,
        "label" => __('Danh mục video', 'tu'),
        "singular_label" => __('Danh mục', 'tu'),
        "rewrite" => array('slug' => __('danh-muc-video', 'tu'), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * RETRIEVING FUNCTIONS
 */

/**
 * Getting video with Pagination
 * @param int $categoryId
 * @param int $page
 * @param int $post_per_page
 * @return WP_Query
 */
function tu_get_video_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'video',
        'order' => 'DESC',
        'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['video_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'video_category',
            'field' => 'id',
            'terms' => $options['video_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * Each video with popup on click
 */
function tu_display_each_video_popup() {
    $post_id = get_the_ID();
    $video_youtube_code = get_post_meta($post_id, 'video_youtube_url', true);
    ?>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="videos-item">
            <div class="frame-video">
                <iframe width="1280" height="720" src="https://www.youtube.com/embed/<?php echo $video_youtube_code; ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                <a href="javascript:void(0);" class="close"><i class="fa fa-times"></i></a>
            </div>
    <?php echo get_the_post_thumbnail($post_id, 'news-thumb'); ?>
            <a target="_blank" href="https://www.youtube.com/embed/<?php echo $video_youtube_code; ?>?rel=0&amp;showinfo=0" class="fancybox fancybox.iframe video-button-play"><i class="fa fa-play-circle" aria-hidden="true"></i></a>

        </div>
        <h4 class="videos-item-title"><?php echo get_the_title(); ?></h4>

    </div>
    <?php
}

/**
 * Each video with normal style
 */
function tu_display_each_video() {
    $post_id = get_the_ID();
    $permalink = get_permalink($post_id);
    $excerpt = wp_trim_words(get_the_excerpt(), 25);
    $thumbnail_url = get_the_post_thumbnail_url($post_id);
    ?>
    <div class="video-item">
        <a class="thumb" href="<?php echo $permalink; ?>" style="background: url('<?php echo tu_no_image_url($thumbnail_url); ?>');"></a>
        <div class="info">
            <h3 class="title"><a href="<?php echo $permalink; ?>"><?php the_title(); ?></a></h3>
            <div class="meta"><?php echo $excerpt; ?></div>
        </div>
    </div>
    <?php
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_video');

function add_metabox_video() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_video_general($post) {
        $post_id = $post->ID;
        $video_is_hot = get_post_meta($post_id, 'video_is_hot', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_video'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="video_is_hot"><?php _e('Video nổi bật', 'tu') ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="video_is_hot" name="video_is_hot"
                               value="1" <?php if ($video_is_hot == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_video_general', 'Thông tin cơ bản', 'display_metabox_video_general', 'video', 'normal', 'high'
    );

    function display_metabox_video_youtube($post) {
        $post_id = $post->ID;
        $video_youtube_url = get_post_meta($post_id, 'video_youtube_url', true);
        ?>
        <table class="form-table">
            <tbody>
                <tr>
                    <td>
                        <input id="video_youtube_url" type="text" class="text" name="video_youtube_url" value="<?php if (isset($video_youtube_url)) echo $video_youtube_url; ?>" placeholder="" />
                        <input id="video_youtube_preview" type="button" name="publish" id="publish" class="button button-primary" value="Xem trước">
                    </td>
                </tr>
                <tr>
                    <td id="video_youtube_frame_outer"></td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                jQuery('#video_youtube_preview').click(function () {
                    var url = jQuery('#video_youtube_url').val();
                    var tmp1 = url.split("?");
                    var code = '';

                    if (tmp1.length > 1) {
                        var tmp2 = tmp1[1].split("=");
                        code = tmp2[1];
                    } else {
                        code = url;
                    }

                    jQuery('#video_youtube_url').val(code);
                    jQuery('#video_youtube_frame_outer').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + code + '" frameborder="0" allowfullscreen></iframe>');

                    return false;
                });

                var code = "<?php if (isset($video_youtube_url) && $video_youtube_url) echo $video_youtube_url; ?>";
                if (code) {
                    jQuery('#video_youtube_preview').click();
                }
            });
        </script>
        <?php
    }

    add_meta_box(
            'display_metabox_video_youtube', 'Youtube (Đường dẫn hoặc mã Youtube)', 'display_metabox_video_youtube', 'video', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_video');

function save_metabox_video($post_id) {
    if (get_post_type() == 'video' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_video')) {

        if (isset($_POST['video_is_hot'])) {
            $video_is_hot = (int) $_POST['video_is_hot'] ? 'true' : 'false';
            update_post_meta($post_id, 'video_is_hot', $video_is_hot);
        }

        if (isset($_POST['video_youtube_url'])) {
            update_post_meta($post_id, 'video_youtube_url', sanitize_text_field($_POST['video_youtube_url']));
        }
    }
}
