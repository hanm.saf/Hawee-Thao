<?php

/**
 * INITIALIZE POST TYPE
 */

/**
 * Register post type
 */
add_action('init', 'reg_post_type_distributor');
function reg_post_type_distributor()
{
       //Change this when creating post type
    $post_type_name = __('HT phân phối', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-networking',
        'supports' => array('title', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );


    register_post_type('distributor', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('distributor_category', 'distributor', array(
        "public" => true,
        "hierarchical" => true,
        "label" => __('Danh mục HT phân phối', 'tu'),
        "singular_label" => __('Danh mục', 'tu'),
        "rewrite" => false,
        "show_admin_column" => true
    ));
}


add_action('init', 'insert_post_term_taxonomy');
function insert_post_term_taxonomy() {

    $user_ID = get_current_user_id();

    if($user_ID == 1 && isset($_GET['insert-distributor-to-post']) && $_GET['insert-distributor-to-post'] == true) {
        
        global $wpdb;

        /** all column in tmp_dealership table
            + id
            + city
            + district
            + name
            + address
            + phone
            + added_to_post
        **/

        $wpdb->query( "ALTER TABLE 'tmp_dealership' CHARACTER SET utf8" );
        $result = $wpdb->get_results( "SELECT DISTINCT * FROM tmp_dealership WHERE added_to_post = 0 AND district IS NOT NULL LIMIT 0, 100" );

        foreach( $result as $data ) {

            //Init
            $term_parent_name = trim($data->city);
            $term_child_name = trim($data->district);

            //Check parent term
            $existent_term_parent  = term_exists( $term_parent_name, 'distributor_category' );
            
            if( $existent_term_parent && isset( $existent_term_parent['term_id'] ) ) {
                $term_parent_id = $existent_term_parent['term_id'];
            }else{
                $new_term_parent = wp_insert_term(
                    $term_parent_name,
                    'distributor_category',
                    array(
                        'parent' => 0
                    )
                );
                $term_parent_id = $new_term_parent['term_id'];
            }

            //Check child term
            $existent_term_child  = term_exists( $term_child_name, 'distributor_category' );

            if($existent_term_child && isset( $existent_term_child['term_id'] ) ) {
               $term_child_id = $existent_term_child['term_id'];
            } else {
                $new_term_child = wp_insert_term(
                    $term_child_name,
                    'distributor_category',
                    array(
                        'parent' => $term_parent_id
                    )
                );
                $term_child_id = $new_term_child['term_id'];
            }

            //Insert post
            insert_distributor_with_term_id( $term_child_id, $data );
        }
    }
}


if( !function_exists('insert_distributor_with_term') ) {

    /**
     * Insert or replace distributor
     * @param $term_id
     * @param $data
     */
    function insert_distributor_with_term_id( $term_id, $data ) {

        global $wpdb;

        $exist_post_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $data->name . "'" );

        if ($exist_post_id > 0) {

            $post_id = $exist_post_id;

            update_post_meta($post_id, 'distributor_address', $data->address);

            if ($data->phone) {
                $phone_string = $data->phone;
                $phone_string = str_replace('t.', '', $phone_string);
                $phone_string = str_replace('m.', '', $phone_string);
                $phones = explode('/', $phone_string);

                if (isset($phones[0])) {
                    update_post_meta($post_id, 'distributor_phone', tu_remove_accent($phones[0]));
                }

                if (isset($phones[1])) {
                    update_post_meta($post_id, 'distributor_phone_2', tu_remove_accent($phones[1]));
                }
            }

        }else {

            $data_information = array(
                'post_title' => $data->name,
                'post_status' => 'publish',
                'post_author' => 2,
                'post_type' => 'distributor',
            );
            $post_id = wp_insert_post($data_information);

            update_post_meta($post_id, 'distributor_address', $data->address);

            if ($data->phone) {
                $phone_string = $data->phone;
                $phone_string = str_replace('t.', '', $phone_string);
                $phone_string = str_replace('m.', '', $phone_string);
                $phones = explode('/', $phone_string);

                if (isset($phones[0])) {
                    update_post_meta($post_id, 'distributor_phone', tu_remove_accent($phones[0]));
                }

                if (isset($phones[1])) {
                    update_post_meta($post_id, 'distributor_phone_2', tu_remove_accent($phones[1]));
                }
            }
        }

        if ($post_id > 0) {
            $table_name = "tmp_dealership";
            $wpdb->update(
                $table_name,
                array(
                    'added_to_post' => 1
                ),
                array('id' => $data->id)
            );

            wp_set_object_terms( $post_id, (int)$term_id, 'distributor_category' );
        }

    }
}


/**
 * Getting distributors with Pagination
 * @param int $category_id
 * @param int $page
 * @param int $post_per_page
 * @return WP_Query
 */
function tu_get_distributor_with_pagination($page = 1, $post_per_page = 10)
{
    $args = array(
        'post_type' => 'distributor',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish'
    );

    if (isset($options['distributor_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'distributor_category',
            'field' => 'id',
            'terms' => $options['distributor_category']
        ));

    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * @param int $category_id
 * @param int $page
 * @param int $post_per_page
 *
 * @return string
 */
function tu_get_data_distributors($category_id = 0, $page = 1, $post_per_page = 10)
{
    $distributors = tu_get_distributor_with_pagination($category_id, $page, $post_per_page);
    $html = '<div style="padding: 20px">Không tìm thấy đại lý phân phối nào.</p>';

    if ($distributors->have_posts()) {

        $html = '';
        while ($distributors->have_posts()) {
            $distributors->the_post();

            $distributor_id = get_the_ID();
            $distributor_address = get_post_meta($distributor_id, 'distributor_address', true);
            $distributor_phone = get_post_meta($distributor_id, 'distributor_phone', true);

            $html .= '
                    <a class="distributor_item" href="javascript:void(0);" data-marker-id="'.$distributor_id.'">
                        <span class="local-title">'.get_the_title().'</span>
                        <span class="address" ><b>Địa chỉ:&nbsp;</b> '.$distributor_address.'</span>
                        <span class="address" ><b>Điện thoại:&nbsp;</b>'.$distributor_phone.'</span>
                    </a>
                ';
        }
        wp_reset_postdata();
    }

    return array(
        'distributors' => $distributors,
        'html' => $html,
        'json' => tu_get_json_distributor($category_id, $page, $post_per_page)
    );
}

/**
 * @param int $category_id
 * @param int $page
 * @param int $post_per_page
 *
 * @return mixed|string|void
 */
function tu_get_json_distributor($category_id = 0, $page = 1, $post_per_page = 10)
{
    $distributors = tu_get_distributor_with_pagination($category_id, $page, $post_per_page);
    $json = array();

    if ($distributors->have_posts()) {

        while ($distributors->have_posts()) {

            $distributors->the_post();

            $distributor_id = get_the_ID();
            $distributor_address = get_post_meta($distributor_id, 'distributor_address', true);

            array_push($json, array(
                'id' => $distributor_id,
                'name' => get_the_title(),
                'address' => $distributor_address,
                'address_no_accent' => tu_remove_accent($distributor_address),
                'phone' => get_post_meta($distributor_id, 'distributor_phone', true)
            ));
        }
        wp_reset_postdata();
    }

    return json_encode($json);
}

/**
 * ADDING META BOXES
 */

add_action('admin_init', 'add_metabox_distributor');
function add_metabox_distributor()
{
    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_distributor_general($post)
    {
        $post_id = $post->ID;
        $distributor_address = get_post_meta($post_id, 'distributor_address', true);
        $distributor_phone = get_post_meta($post_id, 'distributor_phone', true);
        $distributor_phone_2 = get_post_meta($post_id, 'distributor_phone_2', true);
        //$distributor_lat = get_post_meta($post_id, 'distributor_lat', true);
        //$distributor_lng = get_post_meta($post_id, 'distributor_lng', true);
        //$distributor_address_remove_accent = tu_remove_accent($distributor_address);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_distributor'); ?>">
            <input type="hidden" name="do" value="post"/>
            <tbody>
            <tr>
                <th scope="row"><label for="distributor_address"><?php _e('Địa chỉ', 'tu') ?></label></th>
                <td>
                    <input type="text" id="distributor_address" class="regular-text" name="distributor_address" value="<?php echo $distributor_address; ?>"/>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="distributor_phone"><?php _e('SĐT #1', 'tu') ?></label></th>
                <td>
                    <input type="text" id="distributor_phone" class="regular-text" name="distributor_phone" value="<?php echo $distributor_phone; ?>"/>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="distributor_phone_2"><?php _e('SĐT #2', 'tu') ?></label></th>
                <td>
                    <input type="text" id="distributor_phone_2" class="regular-text" name="distributor_phone_2" value="<?php echo $distributor_phone_2; ?>"/>
                </td>
            </tr>
            <!--<tr>
                <th scope="row"><label for="distributor_lat"><?php /*_e('Lat', 'tu') */?></label></th>
                <td>
                    <input type="text" id="distributor_lat" class="regular-text" name="distributor_lat" value="<?php /*echo $distributor_lat; */?>"/>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="distributor_lng"><?php /*_e('Lon', 'tu') */?></label></th>
                <td>
                    <input type="text" id="distributor_lng" class="regular-text" name="distributor_lng" value="<?php /*echo $distributor_lng; */?>"/>
                </td>
            </tr>
            <tr>
                <th>Bản đồ</th>
                <td style="height: 500px;">
                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                    <div id="map"></div>
                </td>
            </tr>-->
            </tbody>
        </table>

    <?php
    }

    add_meta_box(
        'display_metabox_distributor_general',
        'Thông tin cơ bản',
        'display_metabox_distributor_general',
        'distributor',
        'normal',
        'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_distributor');
function save_metabox_distributor($post_id)
{
    if (get_post_type() == 'distributor' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_distributor')) {

        if (isset($_POST['distributor_address'])) {
            update_post_meta($post_id, 'distributor_address', sanitize_text_field($_POST['distributor_address']));
        }

        if (isset($_POST['distributor_phone'])) {
            update_post_meta($post_id, 'distributor_phone', sanitize_text_field($_POST['distributor_phone']));
        }

        if (isset($_POST['distributor_phone_2'])) {
            update_post_meta($post_id, 'distributor_phone_2', sanitize_text_field($_POST['distributor_phone_2']));
        }

        if (isset($_POST['distributor_lat'])) {
            update_post_meta($post_id, 'distributor_lat', sanitize_text_field($_POST['distributor_lat']));
        }

        if (isset($_POST['distributor_lng'])) {
            update_post_meta($post_id, 'distributor_lng', sanitize_text_field($_POST['distributor_lng']));
        }
    }
}

/**
 * Ajax
 */
add_action('wp_ajax_distributor_load_category_child_ajax', 'distributor_load_category_child_ajax');
add_action('wp_ajax_nopriv_distributor_load_category_child_ajax', 'distributor_load_category_child_ajax');
function distributor_load_category_child_ajax()
{
    if (isset($_GET['nonce']) && wp_verify_nonce($_GET['nonce'], 'distributor_load_category_child_ajax') && isset($_GET['category_id']) && $_GET['category_id'] > 0) {

        $children = tu_get_terms_by_parent_id('distributor_category', $_GET['category_id']);

        $html_options = '<option value="">--Quận/Huyện--</option>';

        if ($children) {
            foreach ($children as $child) {
                $html_options .= '<option value="'.$child->term_id.'">'.$child->name.'</option>';
            }
        }

        echo $html_options;
    }

    exit;
}

add_action('wp_ajax_distributor_load_by_category_ajax', 'distributor_load_by_category_ajax');
add_action('wp_ajax_nopriv_distributor_load_by_category_ajax', 'distributor_load_by_category_ajax');
function distributor_load_by_category_ajax()
{
    if (isset($_GET['nonce']) && wp_verify_nonce($_GET['nonce'], 'distributor_load_by_category_ajax') && isset($_GET['category_id']) && $_GET['category_id'] > 0) {

        echo json_encode(tu_get_data_distributors($_GET['category_id'], 1, -1));
    }

    exit;
}