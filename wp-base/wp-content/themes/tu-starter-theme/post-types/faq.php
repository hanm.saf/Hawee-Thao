<?php
/**
 * Author: 
 * Date: 07/02/2017
 * Note: Edit TEXT_DOMAIN
 */
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_faq');

function reg_post_type_faq() {
    //Change this when creating post type
    $post_type_name = __('FAQ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-testimonial',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );
    register_post_type('faq', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('faq_category', 'faq', array(
        "hierarchical" => true,
        "label" => __('Danh mục FAQ', TEXT_DOMAIN),
        "singular_label" => __('Danh mục', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('danh-muc-faq', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * RETRIEVING FUNCTIONS
 */

/**
 * Getting faqs with Pagination
 * @param int $categoryId
 * @param int $page
 * @param int $post_per_page
 * @return WP_Query
 */
function tu_get_faq_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'faq',
        'order' => 'DESC',
        'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['faq_category'])) {
        array_push($args['tax_query'], array(
            'taxonomy' => 'faq_category',
            'field' => 'id',
            'terms' => $options['faq_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_faq');

function add_metabox_faq() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_faq_general($post) {
        $post_id = $post->ID;
        $faq_submit_name = get_post_meta($post_id, 'faq_submit_name', true);
        $faq_submit_email = get_post_meta($post_id, 'faq_submit_email', true);
        $faq_submit_phone = get_post_meta($post_id, 'faq_submit_phone', true);
        $faq_submit_content = get_post_meta($post_id, 'faq_submit_content', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_faq'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="">Họ tên</label></th>
                    <td><?php echo $faq_submit_name; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Email</label></th>
                    <td><?php echo $faq_submit_email; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">SĐT</label></th>
                    <td><?php echo $faq_submit_phone; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Nội dung hỏi</label></th>
                    <td><?php echo $faq_submit_content; ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_faq_general', 'Thông tin FAQ', 'display_metabox_faq_general', 'faq', 'normal', 'high'
    );

    /**
     * Meta box for images
     * @param $post
     */
    function display_metabox_faq_images($post) {
        $post_id = $post->ID;
        $faq_images_ids = get_post_meta($post_id, 'faq_images_ids', true);
        ?>
        <table class="form-table">
            <tbody>
                <tr class="upload-row">
                    <th scope="row">
                        <button type="button" class="button upload-button">Chọn ảnh</button>
                        <input type="hidden" class="upload-files-ids" name="faq_images_ids[0]"
                               value="<?php echo $faq_images_ids; ?>">
                    </th>
                    <td>
                        <div
                            class="upload-files-preview"><?php echo tu_get_images_html_by_ids($faq_images_ids); ?></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                //Multiple Images Upload
                jQuery('.upload-row').delegate('.upload-button', 'click', function (event) {
                    var _this = jQuery(this);

                    var file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select image',
                        library: {},
                        button: {text: 'Select'},
                        //frame: 'post',
                        multiple: true
                    });

                    file_frame.on('select', function () {
                        var attachment_ids = [];
                        attachment = file_frame.state().get('selection').toJSON();
                        console.log(attachment);

                        imgs_html = '';
                        jQuery.each(attachment, function (index, item) {
                            attachment_ids.push(item.id);
                            imgs_html += '<img src="' + item.url + '" />';
                        });

                        var _this_parent = _this.parents('.upload-row');
                        _this_parent.find('.upload-files-ids').val(attachment_ids.join(','));
                        _this_parent.find('.upload-files-preview').html(imgs_html);
                    });

                    file_frame.open();
                });

                //Sortable
                jQuery('.upload-row').find('.upload-files-preview').sortable({
                    start: function (e, ui) {

                    },
                    update: function (e, ui) {
                        var _this_parent = jQuery(this).parents('.upload-row');
                        var new_attachment_ids = [];
                        jQuery.each(_this_parent.find('.upload-files-preview img'), function (index, item) {
                            var this_img_id = jQuery(this).attr("data-img-id");
                            new_attachment_ids.push(this_img_id);
                        });
                        _this_parent.find('.upload-files-ids').val(new_attachment_ids.join(','));
                    }
                });
                jQuery('.upload-row').find('.upload-files-preview').disableSelection();

            });
        </script>
        <style type="text/css">
            .upload-files-preview img {
                width: 24%;
                margin-right: 1%;
            }
        </style>
        <?php
    }

    /* add_meta_box(
      'display_metabox_faq_images',
      'Ảnh FAQ',
      'display_metabox_faq_images',
      'faq',
      'normal',
      'high'
      ); */
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_faq');

function save_metabox_faq($post_id) {
    if (get_post_type() == 'faq' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_faq')) {
        
    }
}

/**
 * AJAX HANDLES
 */
/**
 * Get image src by id
 */
add_action('wp_ajax_faq_submit_question', 'faq_submit_question');
add_action('wp_ajax_nopriv_faq_submit_question', 'faq_submit_question');

function faq_submit_question() {
    exit;
}
