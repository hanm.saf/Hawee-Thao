<?php
/**
 * @author: thanhnq 
 * @date: 06/02/2017
 * @note:   
 * 
 */
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_album');

function reg_post_type_album() {
    //Change this when creating post type
    $post_type_name = __('Thư viện', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('album', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('album_category', array('album'), array(
        "hierarchical" => true,
        "label" => __('Chuyên mục', TEXT_DOMAIN),
        "singular_label" => __('Chuyên mục', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('chuyen-muc', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * BACKEND HANDLES
 */

/** Functions */
function tu_get_album_images_array_by_post_id($post_id = 0) {

    $album_images = get_post_meta($post_id, 'album_images', true);

    if ($album_images) {

        foreach ($album_images as $k => $image) {
            $image['url'] = tu_get_image_src_by_attachment_id($image['id'], 'full');
            $album_images[$k] = $image;
        }

        return $album_images;
    }

    return array();
}

/**
 * RETRIEVING FUNCTIONS
 */

/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_album_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'album',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['album_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'album_category',
            'field' => 'id',
            'terms' => $options['album_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}
/**
 * @param int     $post_per_page
 * @param array   $post__not_in
 * @param array   $options
 *
 * @return WP_Query
 */
function tu_get_album_related($post_per_page = 10,$post__not_in = array(), $options = array()) {
    $args = array(
        'post_type' => 'album',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'post__not_in' => $post__not_in,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['album_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'album_category',
            'field' => 'id',
            'terms' => $options['album_category']
        ));
    }

    $posts = new WP_Query($args);

    return $posts;
}
/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_album');

function add_metabox_album() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_album_general($post) {
        $post_id = $post->ID;
        $album_is_hot = get_post_meta($post_id, 'album_is_hot', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_album'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="album_is_hot"><?php _e('Hình ảnh nổi bật', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="album_is_hot" name="album_is_hot"
                               value="1" <?php if ($album_is_hot == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_album_general', 'Thông tin cơ bản', 'display_metabox_album_general', 'album', 'normal', 'high'
    );

    /**
     * Meta box for images
     * @param $post
     */
    function display_metabox_album_images($post) {
        $post_id = $post->ID;
        ?>
        <table class="form-table album_images-container">
            <tbody>
                <tr>
                    <th>
                        Chọn ảnh
                    </th>
                    <td>
                        <button class="album_images-button button" type="button">Chọn ảnh</button>
                        <ul class="album_images-list tu-meta-box-photo-list ui-sortable"></ul>
                    </td>
                </tr>
            </tbody>
        </table>

        <div id="album_images_edit_popup_container" class="hidden">
            <table id="album_images_edit_popup" class="form-table" data-title="Thông tin ảnh">
                <tr>
                    <th>Ảnh</th>
                    <td><img class="attachment" src="<?php echo tu_get_image_src_by_attachment_id(0); ?>" style="width: auto; max-width: 100%;"></td>
                </tr>
                <tr>
                    <th>Tiêu đề</th>
                    <td><input class="title large-text" type="text" /></td>
                </tr>
                <tr>
                    <th>Đường dẫn</th>
                    <td><input class="external_url large-text" type="text" /></td>
                </tr>
                <tr>
                    <th>Tóm tắt</th>
                    <td><textarea class="description large-text" rows="6"></textarea>
                </tr>
                <tr>
                    <th><input class="guid" type="hidden"></th>
                    <td><button class="button button-primary button-large" type="button">Lưu lại</button></td>
                </tr>
            </table>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                /* Init variables */
                var album_images_container = $(".album_images-container");
                var album_images_list = $(".album_images-list");
                var album_images_button = $('.album_images-button');
                var album_images_edit_popup = $('#album_images_edit_popup');

                /* Sortable handles*/
                album_images_list.each(function (index, el) {
                    $(this).sortable({
                        stop: function () {
                            album_images_update_list_items_name();
                        }
                    }).disableSelection();
                });
                album_images_render_list(JSON.parse('<?php echo tu_json_encode(tu_get_album_images_array_by_post_id($post_id)); ?>'));
                /* Choosing images handles */
                album_images_button.click(function (evt) {

                    var _this = $(this);

                    var fileFrame = wp.media.frames.fileFrame = wp.media({
                        title: 'Select image',
                        library: {type: 'image'},
                        button: {text: 'Select'},
                        multiple: true
                    });

                    fileFrame.on('select', function () {

                        var attachments = fileFrame.state().get('selection').toJSON();

                        $.each(attachments, function (index, item) {
                            album_images_list.append(album_images_get_html_list_item(item));
                            album_images_update_list_items_name();
                        });

                        $(_this).siblings('ul').sortable();
                    });

                    fileFrame.open();
                });

                /* Replace one image handles */
                album_images_list.delegate('.album_images-photo', 'click', function () {

                    var _this = $(this);

                    var fileFrame = wp.media.frames.fileFrame = wp.media({
                        title: 'Select image',
                        library: {type: 'image'},
                        button: {text: 'Select'},
                        multiple: false
                    });

                    var _this_attachment_id = $(_this).siblings('.photo').val();

                    if (_this_attachment_id) {
                        fileFrame.on('open', function () {
                            var selection = fileFrame.state().get('selection');
                            var attachment = wp.media.attachment(_this_attachment_id);
                            attachment.fetch();
                            selection.add(attachment ? [attachment] : []);
                        });
                    }

                    fileFrame.on('select', function () {

                        var attachments = fileFrame.state().get('selection').toJSON();

                        $.each(attachments, function (index, item) {
                            $(_this).attr('src', item.url);
                            $(_this).attr('value', item.id);
                            $(_this).siblings('.attachment').val(item.id);
                        });
                    });

                    fileFrame.open();
                });

                /* Remove image handles */
                album_images_container.delegate('.album_images-remove', 'click', function (evt) {
                    $(this).parent('li').remove();
                    album_images_update_list_items_name();
                });

                /* Edit popup handles */
                $(album_images_list).delegate('.album_images-edit', 'click', function () {

                    album_images_edit_popup.find('.guid').val($(this).parent('li').attr('id'));
                    album_images_edit_popup.find('.attachment').attr('src', $(this).siblings('img').attr('src'));
                    album_images_edit_popup.find('.title').val($(this).siblings('.title').val());
                    album_images_edit_popup.find('.external_url').val($(this).siblings('.external_url').val());
                    album_images_edit_popup.find('.description').val($(this).siblings('.description').val());

                    tb_show(album_images_edit_popup.attr('data-title'), '#TB_inline?width=600&height=550&inlineId=album_images_edit_popup_container');
                });
                $(album_images_edit_popup).find('button').click(function (evt) {

                    var guid = album_images_edit_popup.find('.guid').val();
                    $('#' + guid).children('.title').val(album_images_edit_popup.find('.title').val());
                    $('#' + guid).children('.external_url').val(album_images_edit_popup.find('.external_url').val());
                    $('#' + guid).children('.description').val(album_images_edit_popup.find('.description').val());
                    tb_remove();
                });

                /* Function - Update list item's name */
                function album_images_update_list_items_name() {
                    album_images_list.find('li').each(function (index, el) {
                        console.log(index);
                        $(this).attr('id', 'album_images_' + index);
                        $(this).find('.attachment').attr('name', 'album_images[' + index + '][id]');
                        $(this).find('.title').attr('name', 'album_images[' + index + '][title]');
                        $(this).find('.external_url').attr('name', 'album_images[' + index + '][external_url]');
                        $(this).find('.description').attr('name', 'album_images[' + index + '][description]');
                    });
                }

                /* Function - Render the list */
                function album_images_render_list(attachments) {

                    var attachment_html_forms = '';

                    jQuery.each(attachments, function (index, item) {
                        attachment_html_forms += album_images_get_html_list_item(item);
                    });

                    album_images_list.html(attachment_html_forms);
                    album_images_update_list_items_name();
                }

                function album_images_get_html_list_item(item) {
                    console.log(item);
                    var item_title = item.title || '';
                    var item_external_url = item.external_url || '';
                    var item_description = item.description || '';

                    var html = '<img src="' + item.url + '" class="album_images-photo" title="Change photo" value="' + item.id + '">';
                    html += '<input class="attachment" type="hidden" value="' + item.id + '" />';
                    html += '<input class="title" type="hidden" value="' + item_title + '" />';
                    html += '<input class="external_url" type="hidden" value="' + item_external_url + '" />';
                    html += '<textarea class="description hidden">' + item_description + '</textarea>';
                    html += '<span class="edit album_images-edit dashicons dashicons-edit" title="Edit"></span>';
                    html += '<span class="remove album_images-remove dashicons dashicons-no-alt" title"Remove"></span>';

                    return '<li>' + html + '</li>';
                }

            });
        </script>
        <?php
    }

    add_meta_box(
            'display_metabox_album_images', 'Thư viện ảnh', 'display_metabox_album_images', 'album', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_album');

function save_metabox_album($post_id) {
    if (get_post_type() == 'album' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_album')) {

        $album_is_hot = isset($_POST['album_is_hot']) && (int) $_POST['album_is_hot'] ? 'true' : 'false';
        update_post_meta($post_id, 'album_is_hot', $album_is_hot);

        if (isset($_POST['album_images'])) {
            update_post_meta($post_id, 'album_images', $_POST['album_images']);
        } else {
            delete_post_meta($post_id, 'album_images');
        }
    }
}
