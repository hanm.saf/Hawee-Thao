<?php
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_contact');

function reg_post_type_contact() {
    //Change this when creating post type
    $post_type_name = __('Liên hệ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-feedback',
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false,
         'has_archive' => false,
        'capabilities' => array(
            'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
        ),
        'map_meta_cap' => true
    );

    register_post_type('contact', $args);
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_contact');

function add_metabox_contact() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_contact_general($post) {
        $post_id = $post->ID;
        $contact_email = get_post_meta($post_id, 'contact_email', true);
        $contact_name = get_post_meta($post_id, 'contact_name', true);
        $contact_phone = get_post_meta($post_id, 'contact_phone', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_contact'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="">Họ tên</label></th>
                    <td><?php echo $contact_name; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Email</label></th>
                    <td><?php echo $contact_email; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">SĐT</label></th>
                    <td><?php echo $contact_phone; ?></td>
                </tr>
                <tr>
                    <th scope="row"><label for="">Nội dung</label></th>
                    <td><?php echo $post->post_content; ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_contact_general', 'Thông tin cơ bản', 'display_metabox_contact_general', 'contact', 'normal', 'high'
    );
}
/**
 * NOTIFICATION ----------- ----------- -----------
 */
add_action('admin_menu', 'tu_contact_admin_menu_notification');

function tu_contact_admin_menu_notification() {
    global $menu;
    $contacts = get_posts(array('post_type' => 'contact', 'posts_per_page' => -1, 'post_status' => 'pending'));
    $menu[3][0] .= $contacts ? '&nbsp;<span class="update-plugins count-1" title="' . __('Bạn có', TEXT_DOMAIN) . ' ' . count($contacts) . ' ' . __('thư chưa trả lời', TEXT_DOMAIN) . '"><span class="update-count">' . count($contacts) . '</span></span>' : '';
}
add_action('wp_ajax_contact_submit_ajax', 'contact_submit_ajax');
add_action('wp_ajax_nopriv_contact_submit_ajax', 'contact_submit_ajax');

function contact_submit_ajax() {
    if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'contact_submit_ajax')) {
        echo json_encode(array('success' => false, 'msg' => __('Phiên làm việc đã hết, vui lòng tải lại trang và thử lại.')));
        exit;
    }
    $data = array();
    if (isset($_POST['contact_name'])) {
        $data['contact_name'] = sanitize_text_field($_POST['contact_name']);
    }
    if (isset($_POST['contact_phone'])) {
        $data['contact_phone'] = sanitize_text_field($_POST['contact_phone']);
    }
    if (isset($_POST['contact_email'])) {
        $data['contact_email'] = sanitize_text_field($_POST['contact_email']);
    }
    if (isset($_POST['contact_content'])) {
        $data['contact_content'] = sanitize_text_field($_POST['contact_content']);
    }
    foreach ($data as $d) {
        if (!$d) {
            echo json_encode(array('success' => false, 'msg' => __('Vui lòng điền đầy đủ thông tin.')));
            exit;
        }
    }
    if (!preg_match('/^[0-9_\s]{10,20}+$/i', $_POST['contact_phone'])) {
        echo json_encode(array('success' => false, 'msg' => __('Số điện thoại không hợp lệ')));
        exit;
    }
    if (!is_email($data['contact_email'])) {
        echo json_encode(array('success' => false, 'msg' => 'Email không hợp lệ.'));
        exit;
    }

    $contact_post = array(
        'post_title' => $data['contact_name'] . ' - ' . $data['contact_phone'] . ' - ' . $data['contact_email'],
        'post_content' => $data['contact_content'],
        'post_status' => 'pending',
        'post_type' => 'contact'
    );
    $new_contact_user = wp_insert_post($contact_post);
    if (!is_wp_error($new_contact_user)) {
        update_post_meta($new_contact_user, 'contact_name', $data['contact_name']);
        update_post_meta($new_contact_user, 'contact_phone', $data['contact_phone']);
        update_post_meta($new_contact_user, 'contact_email', $data['contact_email']);

        echo json_encode(array('success' => true, 'msg' => __('Thông tin liên hệ đã được gửi đi, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.')));
        exit;
    } else {
        echo json_encode(array('success' => false, 'msg' => __('Có lỗi xảy ra vui lòng thử lại.')));
        exit;
    }
    exit;
}
