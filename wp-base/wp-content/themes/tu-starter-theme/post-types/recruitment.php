<?php
/**
 * Author:
 * Date: 08/02/2017
 * Note:
 */
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_recruitment');

function reg_post_type_recruitment() {
    //Change this when creating post type
    $post_type_name = __('Tuyển dụng', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );
    register_post_type('recruitment', $args);
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_recruitment');

function add_metabox_recruitment() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_recruitment_general($post) {
        $post_id = $post->ID;
        $recruitment_expiration_date = get_post_meta($post_id, 'recruitment_expiration_date', true);
        $recruitment_workplace = get_post_meta($post_id, 'recruitment_workplace', true);
        ?>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row">
                    <label for="recruitment_workplace">Nơi làm việc</label>
                </th>
                <td>
                    <input type="text" name="recruitment_workplace" value="<?php echo $recruitment_workplace; ?>">
                </td>
            </tr>
            <tr class="form-field">
                <th scope="row">
                    <label for="recruitment_expiration_date">Ngày hết hạn</label>
                </th>
                <td>
                    <input id="datepicker" type="text" name="recruitment_expiration_date" value="<?php echo $recruitment_expiration_date; ?>">
                </td>
            </tr>
        </table>
        <link rel="stylesheet" recruitmentef="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $("#datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                });
            });
        </script>
        <?php
    }

    add_meta_box(
            'display_metabox_recruitment_general', 'Thông tin khác', 'display_metabox_recruitment_general', 'recruitment', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_recruitment');

function save_metabox_recruitment($post_id) {
    if (get_post_type() == 'recruitment') {
        $fields = array(
            'recruitment_expiration_date',
            'recruitment_workplace'
        );

        foreach ($fields as $field) {
            if (isset($_POST[$field])) {
                update_post_meta($post_id, $field, sanitize_text_field($_POST[$field]));
            }
        }
    }
}

function tu_get_recruitment_width_pagination($paged, $posts_per_page = 10) {
    $args = array(
        'post_type' => 'recruitment',
        'posts_per_page' => $posts_per_page,
        'post_status' => 'publish',
        'paged' => $paged
    );
    $post = new WP_Query($args);
    return $post;
}

function tu_get_recruitment_related($post__not_in = array(), $posts_per_page) {
    $args = array(
        'post_type' => 'recruitment',
        'posts_per_page' => $posts_per_page,
        'post__not_in' => $post__not_in,
        'post_status' => 'publish',
    );
    $post = new WP_Query($args);
    return $post;
}

/* Register CV Post Types */
add_action('init', 'reg_post_type_cv');

function reg_post_type_cv() {
    $labels = array(
        'name' => __('CV', TEXT_DOMAIN),
        'singular_name' => __('CV', TEXT_DOMAIN),
        'menu_name' => __('CV', TEXT_DOMAIN),
        'all_items' => __('Danh sách CV', TEXT_DOMAIN),
        'edit_item' => __('Chỉnh sửa CV', TEXT_DOMAIN),
        'new_item' => __('CV mới', TEXT_DOMAIN),
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('No Post Found', TEXT_DOMAIN),
        'not_found_in_trash' => __('No Post Found In Trash', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN)
    );

    $args = array(
        'labels' => $labels,
        'description' => __('CV', TEXT_DOMAIN),
        'public' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'rewrite' => true,
        'menu_position' => 26,
        'show_in_menu' => 'edit.php?post_type=recruitment',
        'menu_icon' => 'dashicons-id-alt',
        'supports' => array('title'),
        'capabilities' => array(
            'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
        ),
        'map_meta_cap' => true
    );

    register_post_type('cv', $args);
}

add_action('admin_init', 'add_metabox_cv');

function add_metabox_cv() {

    function display_metabox_cv_general($post) {
        ?>
        <div class="wrap">
            <table class="form-table">
                <tr>
                    <th scope="row">Ngày nộp hồ sơ</th>
                    <td><?php the_time('d/m/Y'); ?></td>
                </tr>
                <tr>
                    <th scope="row">Tên</th>
                    <td><?php echo get_post_meta(get_the_id(), 'apply_name', true); ?></td>
                </tr>
                <tr>
                    <th scope="row">Số điện thoại</th>
                    <td><?php echo get_post_meta(get_the_id(), 'apply_phone', true); ?></td>
                </tr>
                <tr>
                    <th scope="row">Email</th>
                    <td><a href="mailto:<?php echo get_post_meta(get_the_id(), 'apply_email', true); ?>">
                            <?php echo get_post_meta(get_the_id(), 'apply_email', true); ?>
                        </a></td>
                </tr>
                <tr>
                    <th scope="row">Nội dung</th>
                    <td><?php echo $post->post_content; ?></td>
                </tr>
                <tr>
                    <?php $position = (int) get_post_meta(get_the_id(), 'apply_position', true); ?>
                    <th scope="row">Vị trí ứng tuyển<?php echo $position ?></th>
                    <td>
                        <a href="<?php echo get_edit_post_link($position); ?>"><?php echo get_the_title($position); ?></a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Hồ sơ</th>
                    <td><?php
                        if (get_post_meta(get_the_id(), 'apply_cv', true)) {
                            echo '<p><a href="' . wp_get_attachment_url(get_post_meta(get_the_id(), 'apply_cv', true)) . '">CV</a></p>';
                        }
                        ?></td>
                </tr> 
            </table>       
        </div><?php
    }

    add_meta_box(
            'display_metabox_cv_general', 'Thông tin khác', 'display_metabox_cv_general', 'cv', 'normal', 'high'
    );
}
