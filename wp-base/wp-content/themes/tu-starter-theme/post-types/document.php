<?php
/**
 * Author: 
 * Date: 07/02/2017
 * Note: Edit TEXT_DOMAIN
 */
/**
 * INITIALIZE POST TYPE
 */
/**
 * Register post type
 */
add_action('init', 'reg_post_type_document');

function reg_post_type_document() {
    //Change this when creating post type
    $post_type_name = __('Tài liệu', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN) . ' ' . $post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-media-document',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),
        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );
    register_post_type('document', $args);

    /**
     * Registering Post Type's Taxonomies
     */
    register_taxonomy('document_category', array('document'), array(
        "hierarchical" => true,
        "label" => __('Loại tài liệu', TEXT_DOMAIN),
        "singular_label" => __('Loại tài liệu', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('loai-tai-lieu', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * RETRIEVING FUNCTIONS
 */

/**
 * @param int   $page
 * @param int   $post_per_page
 * @param array $options
 *
 * @return WP_Query
 */
function tu_get_document_with_pagination($page = 1, $post_per_page = 10, $options = array()) {
    $args = array(
        'post_type' => 'document',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['document_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'document_category',
            'field' => 'id',
            'terms' => $options['document_category']
        ));
    }

    if (isset($options['class_course'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'class_course',
            'field' => 'id',
            'terms' => $options['class_course']
        ));
    }

    if (count($args['tax_query']) > 1) {
        
    }

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_document');

function add_metabox_document() {

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_document_general($post) {
        $post_id = $post->ID;
        $document_is_hot = get_post_meta($post_id, 'document_is_hot', true);
        $document_author = get_post_meta($post_id, 'document_author', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_document'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="document_is_hot"><?php _e('Tài liệu nổi bật', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="document_is_hot" name="document_is_hot"
                               value="1" <?php if ($document_is_hot == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="document_author"><?php _e('Tác giả', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="text" id="document_author" class="regular-text" name="document_author" value="<?php echo $document_author; ?>"/>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'display_metabox_document_general', 'Thông tin cơ bản', 'display_metabox_document_general', 'document', 'normal', 'high'
    );

    /**
     * Meta box for general information
     * @param $post
     */
    function display_metabox_document_file($post) {
        $post_id = $post->ID;
        $document_file_id = get_post_meta($post_id, 'document_file_id', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_metabox_document'); ?>">
            <input id="document_file_id" type="hidden" name="document_file_id" value="<?php echo $document_file_id ? (int) $document_file_id : null; ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="document_upload_file"><?php _e('File đính kèm', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <div>
                            <button type="button" id="document_upload_file" class="button">Tải lên File</button>
                            <span id="document_file_name" style="line-height: 30px; padding: 0px 5px;"><?php echo $document_file_id > 0 ? basename(get_post($document_file_id)->guid) : 'No file selected'; ?></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <script>
            jQuery(document).ready(function ($) {

                $('#document_upload_file').click(function (event) {

                    var fileFrame = wp.media.frames.fileFrame = wp.media({
                        title: 'Select file',
                        library: {type: 'application/pdf'},
                        button: {text: 'Select'},
                        multiple: false
                    });

                    fileFrame.on('select', function () {
                        file = fileFrame.state().get('selection').first().toJSON();
                        $('#document_file_id').val(file.id);
                        $('#document_file_name').text(file.filename + ' (' + file.filesizeHumanReadable + ')');
                    });

                    fileFrame.open();
                });

            });
        </script>
        <?php
    }

    add_meta_box(
            'display_metabox_document_file', 'Files', 'display_metabox_document_file', 'document', 'normal', 'high'
    );
}

/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_document');

function save_metabox_document($post_id) {
    if (get_post_type() == 'document' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_metabox_document')) {

        $document_is_hot = isset($_POST['document_is_hot']) && (int) $_POST['document_is_hot'] ? 'true' : 'false';
        update_post_meta($post_id, 'document_is_hot', $document_is_hot);

        if (isset($_POST['document_author'])) {
            update_post_meta($post_id, 'document_author', sanitize_text_field($_POST['document_author']));
        }

        if (isset($_POST['document_file_id'])) {
            update_post_meta($post_id, 'document_file_id', (int) sanitize_text_field($_POST['document_file_id']));
        }
    }
}
